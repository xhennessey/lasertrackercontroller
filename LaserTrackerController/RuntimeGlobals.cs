﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMF.Tracker;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms.VisualStyles;
using Excel=Microsoft.Office.Interop.Excel;


namespace LaserTrackerController
{
    internal class RuntimeGlobals
    {
        private static string _angleUnit;
        private static string _lengthUnit;

        public static Connection Connection { get; set; }
        public static Tracker LMFTracker { get; set; }
        public static MainForm MainForm { get; set; }

        public static string AngleUnit { get => _angleUnit; set => _angleUnit = value; }
        public static string LengthUnit { get => _lengthUnit; set => _lengthUnit = value; }

        // 初次尝试将数据写入Excel表格！
        public static void WriteExcel(List<List<string>> results,string filepath,ref string newFilePath)
        {
            //创建Application对象
            Excel.Application excelApp= new Excel.Application();
            //得到Workbook对象，打开文件
            Excel.Workbook excelBook = excelApp.Workbooks.Open(filepath,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            //指定要操作的sheet
            Excel.Worksheet excelSheet = (Excel.Worksheet) excelBook.Sheets[1]; //编号从1开始
            //向相应位置写入数据
            int row = results.Count;
            int col = results[0].Count;
            for (int rowindex = 2; rowindex < row + 2; ++rowindex)
            {
                for (int colindex = 1; colindex < col + 1; ++colindex)
                {
                    excelSheet.Cells[colindex][rowindex] = results[rowindex - 2][colindex - 1];
                }
                excelSheet.Range[$"A{rowindex}:{(char) ('A' + col - 1)}{rowindex}"].HorizontalAlignment = -4131;
            }
            //调整列宽
            Excel.Range columns = excelSheet.Columns;
            //columns.AutoFit(); //自动调整列宽
            columns.ColumnWidth = 18;
            //设置时间栏的显示格式
            Excel.Range TimeColumns = excelSheet.Range[$"I1:I{col + 1}"];
            TimeColumns.NumberFormat = "yyyy/m/d h:mm:ss";
            //保存WorkBook
            excelBook.Save();
            //excelBook.SaveAs(newfilepath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
            //    Excel.XlSaveAsAccessMode.xlNoChange, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
            //    Missing.Value);
            //从内存中关闭excel对象
            excelSheet = null;
            excelBook.Close();
            excelBook = null;
            //关闭excel提示框
            excelApp.DisplayAlerts = false;
            //Excel从内存中退出
            excelApp.Quit();
            excelApp = null;
            newFilePath = filepath + $"_{row}Measurements";
        }
        /*
        //更改文件名称
        public static void ChangeFileName(string filePath,string newFilePath)
        {
            if (filePath == null || newFilePath == null) return;
            File.Move(filePath + ".xlsx", newFilePath + ".xlsx");//这里filepath必须加上后缀名".xlsx"，否则找不到文件
            Excel.Application excel1=new Excel.Application();
            Excel.Workbook workBook1 = excel1.Workbooks.Open(newFilePath, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            Excel.Worksheet workSheet1 = workBook1.Sheets[1];
            Excel.Range timeColumns = workSheet1.Range[$"I1:I1000"];
            timeColumns.NumberFormat = "yyyy/m/d h:mm:ss";

            workBook1.Save();
            workBook1.Close();

            excel1.DisplayAlerts = false;
            excel1.Quit();
        }
        */
        //创建Excel文件
        public static string CreatExcel()
        {
            //指定模板路径
            string path = System.Windows.Forms.Application.StartupPath;
            //创建excel对象
            Excel.Application excelAPP=new Excel.Application();
            excelAPP.UserControl = true;
            //新建工作簿并加入模板
            Excel.Workbook excelWorkBook =
                excelAPP.Workbooks.Add(Missing.Value);
            Excel.Worksheet excelWorkSheet = (Excel.Worksheet) excelWorkBook.Sheets[1];
            string[] sheetItems = { "Id","x", "y", "z", "q0", "q1", "q2", "q3", "MesureTime", "Face", "CStype" };
            const int sheetcol = 11;
            for (int colIndex = 1; colIndex < sheetcol + 1; ++colIndex)
            {
                excelWorkSheet.Cells[colIndex][1] = sheetItems[colIndex - 1];
            }
            char endindex = (char) ('A' + sheetcol - 1);

            excelWorkSheet.Range[$"A1:{endindex}1"].HorizontalAlignment = -4131;

            //保存新建的工作簿
            string savePath = @path + "\\MeasurementResults" + DateTime.Now.ToString("yyyyMMddHHmmss");//大写M是指month，小写m是指minute，大写H是指hour
            excelWorkBook.SaveAs(savePath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Excel.XlSaveAsAccessMode.xlNoChange, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                Missing.Value);
            //关闭表格
            excelWorkSheet = null;
            //关闭工作簿注销Excel对象
            excelWorkBook.Close();
            excelWorkBook = null;
            excelAPP.Quit();
            excelAPP = null;
            return savePath;
        }
        //打开已有EXCEL文件
        public static void OpenExcelFile(string path)
        {
            System.Diagnostics.Process.Start(path);
        }
    }
}
