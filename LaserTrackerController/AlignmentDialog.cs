﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker.TrackerAlignments;
using LMF.Tracker;
using LMF.Tracker.MeasurementResults;
using LMF.Tracker.ErrorHandling;

namespace LaserTrackerController
{
    public partial class AlignmentDialog : Form
    {
        //Alignment对齐功能用于转站的情况下，将Tracker的测量数据Actual值与参考系下的Nominal值相互转化
        //一般用不到
        private AlignmentInput _GenericInput;
        private AlignmentOrientationInput _orientationInput;
        private AlignmentTransformationInput _transformationInput;

        private double _scale;
        private double _scaleset;

        public AlignmentOrientationResult OrientationResult { get; private set; }
        public AlignmentTransformationResult TransformationResult { get; private set; }
        
        public void SetAlignmentModes()
        {
        }
        public void ClearAlignmentModes()
        {
        }
        
        public AlignmentDialog()
        {
            InitializeComponent();
            _GenericInput = new AlignmentInput();
            _scale = 1.0;
            _scaleset = _GenericInput.UnknownAccuracy;
            InitializeAlignment();
        }

        private void CopyOrientationInput()
        {
            _orientationInput=new AlignmentOrientationInput();

            foreach (var point in _GenericInput.Points)
            {
                _orientationInput.Points.AddPoint(
                    point.Nominal.CoordinateX.ValueInBaseUnits,
                    point.Nominal.CoordinateY.ValueInBaseUnits,
                    point.Nominal.CoordinateZ.ValueInBaseUnits,
                    point.Nominal.CoordinateX.AccuracyInBaseUnits,
                    point.Nominal.CoordinateY.AccuracyInBaseUnits,
                    point.Nominal.CoordinateZ.AccuracyInBaseUnits,
                    point.Measurement.CoordinateX.ValueInBaseUnits,
                    point.Measurement.CoordinateY.ValueInBaseUnits,
                    point.Measurement.CoordinateZ.ValueInBaseUnits,
                    point.Measurement.CoordinateX.AccuracyInBaseUnits,
                    point.Measurement.CoordinateY.AccuracyInBaseUnits,
                    point.Measurement.CoordinateZ.AccuracyInBaseUnits);
            }
            _orientationInput.SetTranslationX(_GenericInput.TranslationX.ValueInBaseUnits,_GenericInput.TranslationX.AccuracyInBaseUnits);
            _orientationInput.SetTranslationY(_GenericInput.TranslationY.ValueInBaseUnits,_GenericInput.TranslationY.AccuracyInBaseUnits);
            _orientationInput.SetTranslationZ(_GenericInput.TranslationZ.ValueInBaseUnits,_GenericInput.TranslationZ.AccuracyInBaseUnits);
            _orientationInput.SetRotationX(_GenericInput.RotationX.ValueInBaseUnits,_GenericInput.RotationX.AccuracyInBaseUnits);
            _orientationInput.SetRotationY(_GenericInput.RotationY.ValueInBaseUnits,_GenericInput.RotationY.AccuracyInBaseUnits);
            _orientationInput.SetRotationZ(_GenericInput.RotationZ.ValueInBaseUnits,_GenericInput.RotationZ.AccuracyInBaseUnits);

        }

        private void CopyTransformationInput()
        {
            _transformationInput=new AlignmentTransformationInput();
            foreach (var point in _GenericInput.Points)
            {
                _transformationInput.Points.AddPoint(
                    point.Nominal.CoordinateX.ValueInBaseUnits,
                    point.Nominal.CoordinateY.ValueInBaseUnits,
                    point.Nominal.CoordinateZ.ValueInBaseUnits,
                    point.Nominal.CoordinateX.AccuracyInBaseUnits,
                    point.Nominal.CoordinateY.AccuracyInBaseUnits,
                    point.Nominal.CoordinateZ.AccuracyInBaseUnits,
                    point.Measurement.CoordinateX.ValueInBaseUnits,
                    point.Measurement.CoordinateY.ValueInBaseUnits,
                    point.Measurement.CoordinateZ.ValueInBaseUnits,
                    point.Measurement.CoordinateX.AccuracyInBaseUnits,
                    point.Measurement.CoordinateY.AccuracyInBaseUnits,
                    point.Measurement.CoordinateZ.AccuracyInBaseUnits);
            }
            _transformationInput.SetTranslationX(_GenericInput.TranslationX.ValueInBaseUnits, _GenericInput.TranslationX.AccuracyInBaseUnits);
            _transformationInput.SetTranslationY(_GenericInput.TranslationY.ValueInBaseUnits, _GenericInput.TranslationY.AccuracyInBaseUnits);
            _transformationInput.SetTranslationZ(_GenericInput.TranslationZ.ValueInBaseUnits, _GenericInput.TranslationZ.AccuracyInBaseUnits);
            _transformationInput.SetRotationX(_GenericInput.RotationX.ValueInBaseUnits, _GenericInput.RotationX.AccuracyInBaseUnits);
            _transformationInput.SetRotationY(_GenericInput.RotationY.ValueInBaseUnits, _GenericInput.RotationY.AccuracyInBaseUnits);
            _transformationInput.SetRotationZ(_GenericInput.RotationZ.ValueInBaseUnits, _GenericInput.RotationZ.AccuracyInBaseUnits);
            _transformationInput.SetScale(_scale,_scaleset);
        }

        private void SetScale(double scale, double scaleset)
        {
            _scale = scale;
            _scaleset = scaleset;
        }

        private void CalculateOrientation()
        {
            try
            {
                CopyOrientationInput();
                OrientationResult = RuntimeGlobals.LMFTracker.TrackerAlignment.CalculateOrientation(_orientationInput);
            }
            catch (Exception ex)
            {
                MessageBox.Show("CalculateOrientationWrong:\n"+ex.Message.ToString());
                //throw;
            }
        }

        private void CalculateTransformation()
        {
            try
            {
                CopyTransformationInput();
                TransformationResult =
                    RuntimeGlobals.LMFTracker.TrackerAlignment.CalculateTransformation(_transformationInput);
            }
            catch (Exception ex)
            {
                MessageBox.Show("CalculateTransformation:\n" + ex.Message);
                //throw;
            }
        }

        public void SetAsTransformation()
        {
            if (TransformationResult != null)
            {
                RuntimeGlobals.LMFTracker.Settings.SetCalculatedTransformation(TransformationResult);
            }
        }

        public void SetAsOrientation()
        {
            if (OrientationResult != null)
            {
                RuntimeGlobals.LMFTracker.Settings.SetCalculatedOrientation(OrientationResult);
            }
        }

        public void ClearOrientation()
        {
            RuntimeGlobals.LMFTracker.Settings.SetOrientation(0,0,0,0,0,0,0);
        }

        public void ClearTransformation()
        {
            RuntimeGlobals.LMFTracker.Settings.SetTransformation(0,0,0,0,0,0,0,1);
        }

        public void Clear()
        {
            _GenericInput=new AlignmentInput();
        }

        public void AddConstraint(double transX, double transY, double transZ, double rotX, double rotY, double rotZ,
            double scale, double accTransX, double accTransY, double accTransZ, double accRotX, double accRotY,
            double accRotZ, double accScale)
        {
            _GenericInput.SetTranslationX(transX,accTransX);
            _GenericInput.SetTranslationY(transY, accTransY);
            _GenericInput.SetTranslationZ(transZ, accTransZ);
            _GenericInput.SetRotationX(rotX, accRotX);
            _GenericInput.SetRotationY(rotY, accRotY);
            _GenericInput.SetRotationZ(rotZ, accRotZ);
            SetScale(scale,accScale);
        }

        public void AddPoint(double nominalX, double nominalY, double nominalZ, double nominalAccX, double nominalAccY,
            double nominalAccZ, double measX, double measY, double measZ, double measAccX, double measAccY,
            double measAccZ)
        {
            _GenericInput.Points.AddPoint(nominalX, nominalY, nominalZ, nominalAccX, nominalAccY, nominalAccZ, measX,
                measY, measZ, measAccX, measAccY, measAccZ);
        }

        public void AddPointFromMeas(double nominalX, double nominalY, double nominalZ, double nominalAccX, double nominalAccY,
            double nominalAccZ, StationaryMeasurement3D meas3D)
        {
            _GenericInput.Points.AddPointFromMeasurement(nominalX, nominalY, nominalZ, nominalAccX, nominalAccY, nominalAccZ,meas3D);
        }

        public int GetNumberOfPoints()
        {
            return _GenericInput.Points.Count;
        }

        //****************以下为事件处理程序*************
        //关闭窗体
        private void Close_Alignment(object sender, EventArgs e)
        {
            this.Close();
        }
        //计算、设置及清除Orientation的相关信息
        private void CalculateOrienbt_Click(object sender, EventArgs e)
        {
            CalculateOrientation();
            CreatOutPutFromOrientation();

            SetOrientationbt.Enabled = true;
            ClearOrienbt.Enabled = true;
        }

        private void CreatOutPutFromOrientation()
        {
            AlignmentOutputlst.Items.Clear();
            if (OrientationResult != null)
            {
                AlignmentOutputlst.Items.Add(
                    "RMS: " + OrientationResult.RMS.ToString("0.000000", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("CoordinateType: " + OrientationResult.Orientation.CoordinateType);
                AlignmentOutputlst.Items.Add("RotationType: " + OrientationResult.Orientation.RotationType);
                AlignmentOutputlst.Items.Add("Number of Points: " + OrientationResult.Points.Count);

                AlignmentOutputlst.Items.Add("*************Params*************");
                AlignmentOutputlst.Items.Add("Translation X [m]: " + OrientationResult.Orientation.Translation1.ValueInBaseUnits.ToString("G8",CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Translation Y [m]: " + OrientationResult.Orientation.Translation2.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Translation Z [m]: " + OrientationResult.Orientation.Translation3.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));

                AlignmentOutputlst.Items.Add("Rotation X [rad]: " + OrientationResult.Orientation.Rotation1.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Rotation Y [rad]: " + OrientationResult.Orientation.Rotation2.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Rotation Z [rad]: " + OrientationResult.Orientation.Rotation3.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                //add residuals
                AlignmentOutputlst.Items.Add("*************Residuals***********");
                foreach (var r in OrientationResult.Points)
                {
                    AlignmentOutputlst.Items.Add("X [m]: " + r.Residual.CoordinateX.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tY [m]: " + r.Residual.CoordinateY.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tZ [m]: " + r.Residual.CoordinateZ.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture));
                }

                //add transformed points
                AlignmentOutputlst.Items.Add("-----------Transfomred-----------");
                foreach (var r in OrientationResult.Points)
                {
                    AlignmentOutputlst.Items.Add("X [m]:  " + r.Transformed.CoordinateX.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tY [m]: " + r.Transformed.CoordinateY.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tZ [m]: " + r.Transformed.CoordinateZ.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture));
                }
            }
        }

        private void SetAsOrientation_Click(object sender, EventArgs e)
        {
            SetAsOrientation();
        }

        private void ClearOrientation_Click(object sender, EventArgs e)
        {
            ClearOrientation();
            AlignmentOutputlst.Items.Clear();
        }
        //计算、设置及清除Transformation的相关信息
        private void CalculateTransbt_Click(object sender, EventArgs e)
        {
            CalculateTransformation();
            CreatOutputFromTransformation();

            SetTransformationbt.Enabled = true;
            ClearTransbt.Enabled = true;
        }

        private void CreatOutputFromTransformation()
        {
            AlignmentOutputlst.Items.Clear();
            if (TransformationResult != null)
            {
                AlignmentOutputlst.Items.Add(
                    "RMS: " + TransformationResult.RMS.ToString("0.000000", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("CoordinateType: " + TransformationResult.Transformation.CoordinateType);
                AlignmentOutputlst.Items.Add("RotationType: " + TransformationResult.Transformation.RotationType);
                AlignmentOutputlst.Items.Add("Number of Points: " + TransformationResult.Points.Count);

                AlignmentOutputlst.Items.Add("*************Params*************");
                AlignmentOutputlst.Items.Add("Translation X [m]: " + TransformationResult.Transformation.Translation1.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Translation Y [m]: " + TransformationResult.Transformation.Translation2.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Translation Z [m]: " + TransformationResult.Transformation.Translation3.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));

                AlignmentOutputlst.Items.Add("Rotation X [rad]: " + TransformationResult.Transformation.Rotation1.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Rotation Y [rad]: " + TransformationResult.Transformation.Rotation2.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Rotation Z [rad]: " + TransformationResult.Transformation.Rotation3.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                AlignmentOutputlst.Items.Add("Scale: " + TransformationResult.Transformation.Scale.ValueInBaseUnits.ToString("G8", CultureInfo.InvariantCulture));
                
                //add residuals
                AlignmentOutputlst.Items.Add("*************Residuals***********");
                foreach (var r in TransformationResult.Points)
                {
                    AlignmentOutputlst.Items.Add("X [m]: " + r.Residual.CoordinateX.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tY [m]: " + r.Residual.CoordinateY.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tZ [m]: " + r.Residual.CoordinateZ.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture));
                }

                //add transformed points
                AlignmentOutputlst.Items.Add("-----------Transfomred-----------");
                foreach (var r in TransformationResult.Points)
                {
                    AlignmentOutputlst.Items.Add("X [m]:  " + r.Transformed.CoordinateX.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tY [m]: " + r.Transformed.CoordinateY.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture) +
                                               "\tZ [m]: " + r.Transformed.CoordinateZ.ValueInBaseUnits.ToString("0.000000", CultureInfo.InvariantCulture));
                }
            }
        }

        private void SetAsTransformation_Click(object sender, EventArgs e)
        {
            SetAsTransformation();
        }

        private void ClearTransformation_Click(object sender, EventArgs e)
        {
            ClearTransformation();
            AlignmentOutputlst.Items.Clear();
        }
        //AddPoint
        private void AddPointbt_Click(object sender, EventArgs e)
        {
            AddPoint(
                double.Parse(NominalXbx.Text),
                double.Parse(NominalYbx.Text),
                double.Parse(NominalZbx.Text),
                double.Parse(NominalXaccbx.Text),
                double.Parse(NominalYaccbx.Text),
                double.Parse(NominalZaccbx.Text),
                double.Parse(MeasXbx.Text),
                double.Parse(MeasYbx.Text),
                double.Parse(MeasZbx.Text),
                double.Parse(MeasXaccbx.Text),
                double.Parse(MeasYaccbx.Text),
                double.Parse(MeasZaccbx.Text));
            Pointstb.Text = GetNumberOfPoints().ToString();
        }
        //AddPointFromMeas
        private void AddPointFromMeas_Click(object sender, EventArgs e)
        {
            Measurement meas = null;
            if (RuntimeGlobals.MainForm.IsStationaryMeasurement(ref meas))
            {
                AddPointFromMeas(
                    double.Parse(NominalXbx.Text),
                    double.Parse(NominalYbx.Text),
                    double.Parse(NominalZbx.Text),
                    double.Parse(NominalXaccbx.Text),
                    double.Parse(NominalYaccbx.Text),
                    double.Parse(NominalZaccbx.Text),
                    meas as StationaryMeasurement3D);
                Pointstb.Text = GetNumberOfPoints().ToString();
            }
            else
            {
                    throw new LmfException(12016);
            }
        }
        //Clear
        private void ClearPoint_CLick(object sender, EventArgs e)
        {
            //新建一个Alignment对象
            Clear();
            InitializeAlignment();

        }
        //初始化窗体相应控件的值
        private void InitializeAlignment()
        {
            NominalXbx.Text = "0";
            NominalYbx.Text = "0";
            NominalZbx.Text = "0";
            NominalXaccbx.Text = "0";
            NominalYaccbx.Text = "0";
            NominalZaccbx.Text = "0";
            MeasXbx.Text = "0";
            MeasYbx.Text = "0";
            MeasZbx.Text = "0";
            MeasXaccbx.Text = "1E-5";
            MeasYaccbx.Text = "1E-5";
            MeasZaccbx.Text = "1E-5";
            
            Pointstb.Text = GetNumberOfPoints().ToString();

            SetOrientationbt.Enabled = false;
            ClearOrienbt.Enabled = false;
            SetTransformationbt.Enabled = false;
            ClearTransbt.Enabled = false;

            ConsTxtb.Text = "0";
            ConsTytb.Text = "0";
            ConsTztb.Text = "0";
            ConsRxcb.Text = "0";
            ConsRycb.Text = "0";
            ConsRzcb.Text = "0";
            ConsScalecb.Text = "1.0";
            ConsTxcbx.SelectedIndex = 0;
            ConsTycbx.SelectedIndex = 0;
            ConsTzcbx.SelectedIndex = 0;
            ConsRxcbx.SelectedIndex = 0;
            ConsRycbx.SelectedIndex = 0;
            ConsRzcbx.SelectedIndex = 0;
            ConsScalecbx.SelectedIndex = 0;

            AlignmentOutputlst.Items.Clear();
        }
        //添加约束
        private void AddConstraint_Click(object sender, EventArgs e)
        {
            AddConstraint(
                double.Parse(ConsTxtb.Text),
                double.Parse(ConsTytb.Text),
                double.Parse(ConsTztb.Text),
                double.Parse(ConsRxcb.Text),
                double.Parse(ConsRycb.Text),
                double.Parse(ConsRzcb.Text),
                double.Parse(ConsScalecb.Text),
                FromComboBoxIndexToAccuracy(ConsTxcbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsTycbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsTzcbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsRxcbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsRycbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsRzcbx.SelectedIndex),
                FromComboBoxIndexToAccuracy(ConsScalecbx.SelectedIndex));
        }

        private double FromComboBoxIndexToAccuracy(int index)
        {
            var input = new AlignmentInput();
            double accuracy = 0;

            switch (index)
            {
                case 0:
                    accuracy = input.UnknownAccuracy;
                    break;
                case 1:
                    accuracy = input.FixedAccuracy;
                    break;
                case 2:
                    accuracy = input.ApproximateAccuracy;
                    break;
                default:
                    throw new NotImplementedException();
            }
            return accuracy;
        }

    }
}
