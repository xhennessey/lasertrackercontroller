﻿namespace LaserTrackerController
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lengthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.angleUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.degreeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cartesianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sphericalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cylindricalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotationTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quartersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rPYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotationAnglesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alignmentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oVCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oVCToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Measuringtab2 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Measurementgrp = new System.Windows.Forms.GroupBox();
            this.WriteToExcelbt = new System.Windows.Forms.Button();
            this.StopMeasurebt = new System.Windows.Forms.Button();
            this.MeasureStatSyncbt = new System.Windows.Forms.Button();
            this.Startmeasurebt = new System.Windows.Forms.Button();
            this.MeasurementResultslst = new System.Windows.Forms.ListBox();
            this.MeasureSettings = new System.Windows.Forms.GroupBox();
            this.MeasureSettingsgrp = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Precise = new System.Windows.Forms.RadioButton();
            this.Standard = new System.Windows.Forms.RadioButton();
            this.Fastbt = new System.Windows.Forms.RadioButton();
            this.ContDisttxt = new System.Windows.Forms.TextBox();
            this.ContTimetxt = new System.Windows.Forms.TextBox();
            this.ContTimePro = new System.Windows.Forms.Label();
            this.ContDistPro = new System.Windows.Forms.Label();
            this.StationaryPro = new System.Windows.Forms.Label();
            this.Profileslb = new System.Windows.Forms.Label();
            this.Profilescmb = new System.Windows.Forms.ComboBox();
            this.Preconditionlst = new System.Windows.Forms.ListBox();
            this.Triggersgrp = new System.Windows.Forms.GroupBox();
            this.RemoteControlcb = new System.Windows.Forms.CheckBox();
            this.MeasOnProbeButtonDn = new System.Windows.Forms.CheckBox();
            this.MeasStablecb = new System.Windows.Forms.CheckBox();
            this.Targetsgrp = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.SelectedTarget = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.PreselectdTarget = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ActiveTips = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Targetscbox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TargetQ3 = new System.Windows.Forms.TextBox();
            this.TargetQ2 = new System.Windows.Forms.TextBox();
            this.TargetQ1 = new System.Windows.Forms.TextBox();
            this.TargetQ0 = new System.Windows.Forms.TextBox();
            this.Targetposz = new System.Windows.Forms.TextBox();
            this.Targetposy = new System.Windows.Forms.TextBox();
            this.Targetposx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Connecttab1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ConnectedResults = new System.Windows.Forms.TextBox();
            this.PositionToStationary = new System.Windows.Forms.GroupBox();
            this.PositionTarget = new System.Windows.Forms.Button();
            this.PtchBox = new System.Windows.Forms.CheckBox();
            this.GoMeasurebt = new System.Windows.Forms.Button();
            this.LockOnTokencBox = new System.Windows.Forms.ComboBox();
            this.LockonToken = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Xptlabel = new System.Windows.Forms.Label();
            this.Yptlabel = new System.Windows.Forms.Label();
            this.Zptlabel = new System.Windows.Forms.Label();
            this.Xptpos = new System.Windows.Forms.TextBox();
            this.Yptpos = new System.Windows.Forms.TextBox();
            this.Zptpos = new System.Windows.Forms.TextBox();
            this.PositionToGroup = new System.Windows.Forms.GroupBox();
            this.Positionto = new System.Windows.Forms.Button();
            this.Ansyncbt = new System.Windows.Forms.Button();
            this.SearchTargetchBox = new System.Windows.Forms.CheckBox();
            this.IsRelativechBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.X_label = new System.Windows.Forms.Label();
            this.Y_label = new System.Windows.Forms.Label();
            this.Z_label = new System.Windows.Forms.Label();
            this.Xpos = new System.Windows.Forms.TextBox();
            this.Ypos = new System.Windows.Forms.TextBox();
            this.Zpos = new System.Windows.Forms.TextBox();
            this.MoveControl = new System.Windows.Forms.GroupBox();
            this.GoBirdBathbt = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MoveSpeed = new System.Windows.Forms.Label();
            this.SpeedValue = new System.Windows.Forms.TextBox();
            this.SpeedUnit = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Rightbt = new System.Windows.Forms.Button();
            this.Downbt = new System.Windows.Forms.Button();
            this.Stopbt = new System.Windows.Forms.Button();
            this.Leftbt = new System.Windows.Forms.Button();
            this.Upbt = new System.Windows.Forms.Button();
            this.Initialize_Disconnect = new System.Windows.Forms.GroupBox();
            this.Disconnectbt = new System.Windows.Forms.Button();
            this.Initializebt = new System.Windows.Forms.Button();
            this.Discover_tracker = new System.Windows.Forms.GroupBox();
            this.ConnectDbt = new System.Windows.Forms.Button();
            this.Discoverbt = new System.Windows.Forms.Button();
            this.ConnectSim = new System.Windows.Forms.GroupBox();
            this.AT960bt = new System.Windows.Forms.Button();
            this.AT930bt = new System.Windows.Forms.Button();
            this.AT901bt = new System.Windows.Forms.Button();
            this.AT403bt = new System.Windows.Forms.Button();
            this.AT402bt = new System.Windows.Forms.Button();
            this.AT401bt = new System.Windows.Forms.Button();
            this.Trackers_list = new System.Windows.Forms.ListBox();
            this.ConnectIP = new System.Windows.Forms.GroupBox();
            this.IPAddress = new System.Windows.Forms.TextBox();
            this.ConnectstaticIP = new System.Windows.Forms.Button();
            this.IPlabel = new System.Windows.Forms.Label();
            this.Connecttabs = new System.Windows.Forms.TabControl();
            this.TrackerErrortab3 = new System.Windows.Forms.TabPage();
            this.Errorsgrp = new System.Windows.Forms.GroupBox();
            this.ErrorDescriptiontb = new System.Windows.Forms.TextBox();
            this.Descriptionbt = new System.Windows.Forms.Button();
            this.ErrorNumbertb = new System.Windows.Forms.TextBox();
            this.Menu.SuspendLayout();
            this.Measuringtab2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Measurementgrp.SuspendLayout();
            this.MeasureSettings.SuspendLayout();
            this.MeasureSettingsgrp.SuspendLayout();
            this.panel4.SuspendLayout();
            this.Triggersgrp.SuspendLayout();
            this.Targetsgrp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.Connecttab1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PositionToStationary.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.PositionToGroup.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.MoveControl.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Initialize_Disconnect.SuspendLayout();
            this.Discover_tracker.SuspendLayout();
            this.ConnectSim.SuspendLayout();
            this.ConnectIP.SuspendLayout();
            this.Connecttabs.SuspendLayout();
            this.TrackerErrortab3.SuspendLayout();
            this.Errorsgrp.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.measureToolStripMenuItem,
            this.alignmentToolStripMenuItem,
            this.oVCToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(834, 25);
            this.Menu.TabIndex = 0;
            this.Menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStripMenuItem,
            this.closeStripMenuItem,
            this.aboutStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newStripMenuItem
            // 
            this.newStripMenuItem.Name = "newStripMenuItem";
            this.newStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.newStripMenuItem.Text = "OpenFile";
            this.newStripMenuItem.Click += new System.EventHandler(this.OpenMeasurementfile_Click);
            // 
            // closeStripMenuItem
            // 
            this.closeStripMenuItem.Name = "closeStripMenuItem";
            this.closeStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.closeStripMenuItem.Text = "Close";
            this.closeStripMenuItem.Click += new System.EventHandler(this.CloseAll_Click);
            // 
            // aboutStripMenuItem
            // 
            this.aboutStripMenuItem.Name = "aboutStripMenuItem";
            this.aboutStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.aboutStripMenuItem.Text = "About";
            this.aboutStripMenuItem.Click += new System.EventHandler(this.ShowAbout_Click);
            // 
            // measureToolStripMenuItem
            // 
            this.measureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unitsToolStripMenuItem,
            this.csTypesToolStripMenuItem,
            this.rotationTypesToolStripMenuItem});
            this.measureToolStripMenuItem.Name = "measureToolStripMenuItem";
            this.measureToolStripMenuItem.Size = new System.Drawing.Size(66, 21);
            this.measureToolStripMenuItem.Text = "Settings";
            // 
            // unitsToolStripMenuItem
            // 
            this.unitsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lengthToolStripMenuItem,
            this.angleUnitToolStripMenuItem});
            this.unitsToolStripMenuItem.Name = "unitsToolStripMenuItem";
            this.unitsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.unitsToolStripMenuItem.Text = "Units";
            // 
            // lengthToolStripMenuItem
            // 
            this.lengthToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mmToolStripMenuItem,
            this.mToolStripMenuItem,
            this.inchToolStripMenuItem});
            this.lengthToolStripMenuItem.Name = "lengthToolStripMenuItem";
            this.lengthToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.lengthToolStripMenuItem.Text = "LengthUnit";
            // 
            // mmToolStripMenuItem
            // 
            this.mmToolStripMenuItem.Name = "mmToolStripMenuItem";
            this.mmToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.mmToolStripMenuItem.Text = "mm";
            this.mmToolStripMenuItem.Click += new System.EventHandler(this.MM_Click);
            // 
            // mToolStripMenuItem
            // 
            this.mToolStripMenuItem.Name = "mToolStripMenuItem";
            this.mToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.mToolStripMenuItem.Text = "m";
            this.mToolStripMenuItem.Click += new System.EventHandler(this.M_Click);
            // 
            // inchToolStripMenuItem
            // 
            this.inchToolStripMenuItem.Name = "inchToolStripMenuItem";
            this.inchToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.inchToolStripMenuItem.Text = "inch";
            this.inchToolStripMenuItem.Click += new System.EventHandler(this.Inch_Click);
            // 
            // angleUnitToolStripMenuItem
            // 
            this.angleUnitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.radianToolStripMenuItem,
            this.degreeToolStripMenuItem,
            this.gonToolStripMenuItem});
            this.angleUnitToolStripMenuItem.Name = "angleUnitToolStripMenuItem";
            this.angleUnitToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.angleUnitToolStripMenuItem.Text = "AngleUnit";
            // 
            // radianToolStripMenuItem
            // 
            this.radianToolStripMenuItem.Name = "radianToolStripMenuItem";
            this.radianToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.radianToolStripMenuItem.Text = "Radian";
            this.radianToolStripMenuItem.Click += new System.EventHandler(this.RadianSet);
            // 
            // degreeToolStripMenuItem
            // 
            this.degreeToolStripMenuItem.Name = "degreeToolStripMenuItem";
            this.degreeToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.degreeToolStripMenuItem.Text = "Degree";
            this.degreeToolStripMenuItem.Click += new System.EventHandler(this.DegreeSet);
            // 
            // gonToolStripMenuItem
            // 
            this.gonToolStripMenuItem.Name = "gonToolStripMenuItem";
            this.gonToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.gonToolStripMenuItem.Text = "Gon";
            this.gonToolStripMenuItem.Click += new System.EventHandler(this.GonSet);
            // 
            // csTypesToolStripMenuItem
            // 
            this.csTypesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cartesianToolStripMenuItem,
            this.sphericalToolStripMenuItem,
            this.cylindricalToolStripMenuItem});
            this.csTypesToolStripMenuItem.Name = "csTypesToolStripMenuItem";
            this.csTypesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.csTypesToolStripMenuItem.Text = "CS types";
            // 
            // cartesianToolStripMenuItem
            // 
            this.cartesianToolStripMenuItem.Name = "cartesianToolStripMenuItem";
            this.cartesianToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.cartesianToolStripMenuItem.Text = "Cartesian";
            this.cartesianToolStripMenuItem.Click += new System.EventHandler(this.CartesianSet);
            // 
            // sphericalToolStripMenuItem
            // 
            this.sphericalToolStripMenuItem.Name = "sphericalToolStripMenuItem";
            this.sphericalToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.sphericalToolStripMenuItem.Text = "Spherical";
            this.sphericalToolStripMenuItem.Click += new System.EventHandler(this.SphericalSet);
            // 
            // cylindricalToolStripMenuItem
            // 
            this.cylindricalToolStripMenuItem.Name = "cylindricalToolStripMenuItem";
            this.cylindricalToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.cylindricalToolStripMenuItem.Text = "Cylindrical";
            this.cylindricalToolStripMenuItem.Click += new System.EventHandler(this.CylindricalSet);
            // 
            // rotationTypesToolStripMenuItem
            // 
            this.rotationTypesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quartersToolStripMenuItem,
            this.rPYToolStripMenuItem,
            this.rotationAnglesToolStripMenuItem});
            this.rotationTypesToolStripMenuItem.Name = "rotationTypesToolStripMenuItem";
            this.rotationTypesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.rotationTypesToolStripMenuItem.Text = "RotationTypes";
            // 
            // quartersToolStripMenuItem
            // 
            this.quartersToolStripMenuItem.Name = "quartersToolStripMenuItem";
            this.quartersToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.quartersToolStripMenuItem.Text = "Quarternions";
            this.quartersToolStripMenuItem.Click += new System.EventHandler(this.RtQuartSet);
            // 
            // rPYToolStripMenuItem
            // 
            this.rPYToolStripMenuItem.Name = "rPYToolStripMenuItem";
            this.rPYToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.rPYToolStripMenuItem.Text = "RPY";
            this.rPYToolStripMenuItem.Click += new System.EventHandler(this.RtRPYSet);
            // 
            // rotationAnglesToolStripMenuItem
            // 
            this.rotationAnglesToolStripMenuItem.Name = "rotationAnglesToolStripMenuItem";
            this.rotationAnglesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.rotationAnglesToolStripMenuItem.Text = "RotationAngles";
            this.rotationAnglesToolStripMenuItem.Click += new System.EventHandler(this.RtAnglesSet);
            // 
            // alignmentToolStripMenuItem
            // 
            this.alignmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alignmentToolStripMenuItem1,
            this.aboutToolStripMenuItem1});
            this.alignmentToolStripMenuItem.Name = "alignmentToolStripMenuItem";
            this.alignmentToolStripMenuItem.Size = new System.Drawing.Size(78, 21);
            this.alignmentToolStripMenuItem.Text = "Alignment";
            // 
            // alignmentToolStripMenuItem1
            // 
            this.alignmentToolStripMenuItem1.Name = "alignmentToolStripMenuItem1";
            this.alignmentToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
            this.alignmentToolStripMenuItem1.Text = "Alignment";
            this.alignmentToolStripMenuItem1.Click += new System.EventHandler(this.AlignmentInitialize);
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.ShowAlignmentAboutDialog_Click);
            // 
            // oVCToolStripMenuItem
            // 
            this.oVCToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oVCToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.oVCToolStripMenuItem.Name = "oVCToolStripMenuItem";
            this.oVCToolStripMenuItem.Size = new System.Drawing.Size(46, 21);
            this.oVCToolStripMenuItem.Text = "OVC";
            // 
            // oVCToolStripMenuItem1
            // 
            this.oVCToolStripMenuItem1.Name = "oVCToolStripMenuItem1";
            this.oVCToolStripMenuItem1.Size = new System.Drawing.Size(111, 22);
            this.oVCToolStripMenuItem1.Text = "OVC";
            this.oVCToolStripMenuItem1.Click += new System.EventHandler(this.OVCFunction);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.OvcAbout_Click);
            // 
            // Measuringtab2
            // 
            this.Measuringtab2.Controls.Add(this.panel3);
            this.Measuringtab2.Location = new System.Drawing.Point(4, 4);
            this.Measuringtab2.Name = "Measuringtab2";
            this.Measuringtab2.Size = new System.Drawing.Size(826, 476);
            this.Measuringtab2.TabIndex = 2;
            this.Measuringtab2.Text = "Measuring";
            this.Measuringtab2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.Measurementgrp);
            this.panel3.Controls.Add(this.MeasureSettings);
            this.panel3.Controls.Add(this.Targetsgrp);
            this.panel3.Location = new System.Drawing.Point(10, 10);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(808, 454);
            this.panel3.TabIndex = 0;
            // 
            // Measurementgrp
            // 
            this.Measurementgrp.Controls.Add(this.WriteToExcelbt);
            this.Measurementgrp.Controls.Add(this.StopMeasurebt);
            this.Measurementgrp.Controls.Add(this.MeasureStatSyncbt);
            this.Measurementgrp.Controls.Add(this.Startmeasurebt);
            this.Measurementgrp.Controls.Add(this.MeasurementResultslst);
            this.Measurementgrp.Location = new System.Drawing.Point(20, 312);
            this.Measurementgrp.Name = "Measurementgrp";
            this.Measurementgrp.Size = new System.Drawing.Size(771, 128);
            this.Measurementgrp.TabIndex = 2;
            this.Measurementgrp.TabStop = false;
            this.Measurementgrp.Text = "Measurement and Results";
            // 
            // WriteToExcelbt
            // 
            this.WriteToExcelbt.Enabled = false;
            this.WriteToExcelbt.Location = new System.Drawing.Point(16, 100);
            this.WriteToExcelbt.Name = "WriteToExcelbt";
            this.WriteToExcelbt.Size = new System.Drawing.Size(125, 23);
            this.WriteToExcelbt.TabIndex = 7;
            this.WriteToExcelbt.Text = "Write to Excel";
            this.WriteToExcelbt.UseVisualStyleBackColor = true;
            this.WriteToExcelbt.Click += new System.EventHandler(this.WriteToExcel_Click);
            // 
            // StopMeasurebt
            // 
            this.StopMeasurebt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StopMeasurebt.Location = new System.Drawing.Point(16, 73);
            this.StopMeasurebt.Name = "StopMeasurebt";
            this.StopMeasurebt.Size = new System.Drawing.Size(125, 25);
            this.StopMeasurebt.TabIndex = 6;
            this.StopMeasurebt.Text = "Stop Measure";
            this.StopMeasurebt.UseVisualStyleBackColor = true;
            this.StopMeasurebt.Click += new System.EventHandler(this.StopMeasure_Click);
            // 
            // MeasureStatSyncbt
            // 
            this.MeasureStatSyncbt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MeasureStatSyncbt.Location = new System.Drawing.Point(16, 47);
            this.MeasureStatSyncbt.Name = "MeasureStatSyncbt";
            this.MeasureStatSyncbt.Size = new System.Drawing.Size(125, 24);
            this.MeasureStatSyncbt.TabIndex = 5;
            this.MeasureStatSyncbt.Text = "Measure Stat Sync";
            this.MeasureStatSyncbt.UseVisualStyleBackColor = true;
            this.MeasureStatSyncbt.Click += new System.EventHandler(this.MeasureStatSync);
            // 
            // Startmeasurebt
            // 
            this.Startmeasurebt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Startmeasurebt.Location = new System.Drawing.Point(16, 20);
            this.Startmeasurebt.Name = "Startmeasurebt";
            this.Startmeasurebt.Size = new System.Drawing.Size(125, 25);
            this.Startmeasurebt.TabIndex = 4;
            this.Startmeasurebt.Text = "Start Measure";
            this.Startmeasurebt.UseVisualStyleBackColor = true;
            this.Startmeasurebt.Click += new System.EventHandler(this.Startmeasure);
            // 
            // MeasurementResultslst
            // 
            this.MeasurementResultslst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MeasurementResultslst.FormattingEnabled = true;
            this.MeasurementResultslst.ItemHeight = 12;
            this.MeasurementResultslst.Location = new System.Drawing.Point(162, 22);
            this.MeasurementResultslst.Name = "MeasurementResultslst";
            this.MeasurementResultslst.Size = new System.Drawing.Size(590, 100);
            this.MeasurementResultslst.TabIndex = 0;
            // 
            // MeasureSettings
            // 
            this.MeasureSettings.Controls.Add(this.MeasureSettingsgrp);
            this.MeasureSettings.Controls.Add(this.Triggersgrp);
            this.MeasureSettings.Location = new System.Drawing.Point(20, 126);
            this.MeasureSettings.Name = "MeasureSettings";
            this.MeasureSettings.Size = new System.Drawing.Size(771, 179);
            this.MeasureSettings.TabIndex = 1;
            this.MeasureSettings.TabStop = false;
            this.MeasureSettings.Text = "MeasureSetting";
            // 
            // MeasureSettingsgrp
            // 
            this.MeasureSettingsgrp.Controls.Add(this.panel4);
            this.MeasureSettingsgrp.Controls.Add(this.Preconditionlst);
            this.MeasureSettingsgrp.Location = new System.Drawing.Point(152, 20);
            this.MeasureSettingsgrp.Name = "MeasureSettingsgrp";
            this.MeasureSettingsgrp.Size = new System.Drawing.Size(619, 149);
            this.MeasureSettingsgrp.TabIndex = 8;
            this.MeasureSettingsgrp.TabStop = false;
            this.MeasureSettingsgrp.Text = "Preconditions Profiles Triggers";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.Precise);
            this.panel4.Controls.Add(this.Standard);
            this.panel4.Controls.Add(this.Fastbt);
            this.panel4.Controls.Add(this.ContDisttxt);
            this.panel4.Controls.Add(this.ContTimetxt);
            this.panel4.Controls.Add(this.ContTimePro);
            this.panel4.Controls.Add(this.ContDistPro);
            this.panel4.Controls.Add(this.StationaryPro);
            this.panel4.Controls.Add(this.Profileslb);
            this.panel4.Controls.Add(this.Profilescmb);
            this.panel4.Location = new System.Drawing.Point(328, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(288, 131);
            this.panel4.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(252, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "ms";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(252, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "mm";
            // 
            // Precise
            // 
            this.Precise.AutoSize = true;
            this.Precise.Location = new System.Drawing.Point(203, 46);
            this.Precise.Name = "Precise";
            this.Precise.Size = new System.Drawing.Size(65, 16);
            this.Precise.TabIndex = 3;
            this.Precise.TabStop = true;
            this.Precise.Text = "Precise";
            this.Precise.UseVisualStyleBackColor = true;
            this.Precise.CheckedChanged += new System.EventHandler(this.Accuracy_Precise);
            // 
            // Standard
            // 
            this.Standard.AutoSize = true;
            this.Standard.Location = new System.Drawing.Point(129, 46);
            this.Standard.Name = "Standard";
            this.Standard.Size = new System.Drawing.Size(71, 16);
            this.Standard.TabIndex = 9;
            this.Standard.TabStop = true;
            this.Standard.Text = "Standard";
            this.Standard.UseVisualStyleBackColor = true;
            this.Standard.CheckedChanged += new System.EventHandler(this.Accuracy_Standard);
            // 
            // Fastbt
            // 
            this.Fastbt.AutoSize = true;
            this.Fastbt.Location = new System.Drawing.Point(79, 46);
            this.Fastbt.Name = "Fastbt";
            this.Fastbt.Size = new System.Drawing.Size(47, 16);
            this.Fastbt.TabIndex = 8;
            this.Fastbt.TabStop = true;
            this.Fastbt.Text = "Fast";
            this.Fastbt.UseVisualStyleBackColor = true;
            this.Fastbt.CheckedChanged += new System.EventHandler(this.Accuracy_Fast);
            // 
            // ContDisttxt
            // 
            this.ContDisttxt.Location = new System.Drawing.Point(79, 71);
            this.ContDisttxt.Name = "ContDisttxt";
            this.ContDisttxt.Size = new System.Drawing.Size(163, 21);
            this.ContDisttxt.TabIndex = 7;
            this.ContDisttxt.TextChanged += new System.EventHandler(this.ContinuousDis);
            // 
            // ContTimetxt
            // 
            this.ContTimetxt.Location = new System.Drawing.Point(79, 101);
            this.ContTimetxt.Name = "ContTimetxt";
            this.ContTimetxt.Size = new System.Drawing.Size(163, 21);
            this.ContTimetxt.TabIndex = 6;
            this.ContTimetxt.TextChanged += new System.EventHandler(this.ContinuousTime);
            // 
            // ContTimePro
            // 
            this.ContTimePro.AutoSize = true;
            this.ContTimePro.Location = new System.Drawing.Point(8, 103);
            this.ContTimePro.Name = "ContTimePro";
            this.ContTimePro.Size = new System.Drawing.Size(59, 12);
            this.ContTimePro.TabIndex = 5;
            this.ContTimePro.Text = "ContTime:";
            // 
            // ContDistPro
            // 
            this.ContDistPro.AutoSize = true;
            this.ContDistPro.Location = new System.Drawing.Point(8, 74);
            this.ContDistPro.Name = "ContDistPro";
            this.ContDistPro.Size = new System.Drawing.Size(59, 12);
            this.ContDistPro.TabIndex = 4;
            this.ContDistPro.Text = "ContDist:";
            // 
            // StationaryPro
            // 
            this.StationaryPro.AutoSize = true;
            this.StationaryPro.Location = new System.Drawing.Point(8, 48);
            this.StationaryPro.Name = "StationaryPro";
            this.StationaryPro.Size = new System.Drawing.Size(71, 12);
            this.StationaryPro.TabIndex = 3;
            this.StationaryPro.Text = "Stationary:";
            // 
            // Profileslb
            // 
            this.Profileslb.AutoSize = true;
            this.Profileslb.Location = new System.Drawing.Point(8, 21);
            this.Profileslb.Name = "Profileslb";
            this.Profileslb.Size = new System.Drawing.Size(59, 12);
            this.Profileslb.TabIndex = 2;
            this.Profileslb.Text = "Profiles:";
            // 
            // Profilescmb
            // 
            this.Profilescmb.FormattingEnabled = true;
            this.Profilescmb.Location = new System.Drawing.Point(79, 18);
            this.Profilescmb.Name = "Profilescmb";
            this.Profilescmb.Size = new System.Drawing.Size(189, 20);
            this.Profilescmb.TabIndex = 1;
            this.Profilescmb.SelectionChangeCommitted += new System.EventHandler(this.ProfileSelected_Click);
            // 
            // Preconditionlst
            // 
            this.Preconditionlst.FormattingEnabled = true;
            this.Preconditionlst.ItemHeight = 12;
            this.Preconditionlst.Location = new System.Drawing.Point(10, 23);
            this.Preconditionlst.Name = "Preconditionlst";
            this.Preconditionlst.Size = new System.Drawing.Size(304, 112);
            this.Preconditionlst.TabIndex = 0;
            // 
            // Triggersgrp
            // 
            this.Triggersgrp.Controls.Add(this.RemoteControlcb);
            this.Triggersgrp.Controls.Add(this.MeasOnProbeButtonDn);
            this.Triggersgrp.Controls.Add(this.MeasStablecb);
            this.Triggersgrp.Location = new System.Drawing.Point(6, 20);
            this.Triggersgrp.Name = "Triggersgrp";
            this.Triggersgrp.Size = new System.Drawing.Size(138, 149);
            this.Triggersgrp.TabIndex = 9;
            this.Triggersgrp.TabStop = false;
            this.Triggersgrp.Text = "Triggers";
            // 
            // RemoteControlcb
            // 
            this.RemoteControlcb.AutoSize = true;
            this.RemoteControlcb.Location = new System.Drawing.Point(13, 101);
            this.RemoteControlcb.Name = "RemoteControlcb";
            this.RemoteControlcb.Size = new System.Drawing.Size(102, 16);
            this.RemoteControlcb.TabIndex = 8;
            this.RemoteControlcb.Text = "RemoteControl";
            this.RemoteControlcb.UseVisualStyleBackColor = true;
            // 
            // MeasOnProbeButtonDn
            // 
            this.MeasOnProbeButtonDn.AutoSize = true;
            this.MeasOnProbeButtonDn.Location = new System.Drawing.Point(13, 40);
            this.MeasOnProbeButtonDn.Name = "MeasOnProbeButtonDn";
            this.MeasOnProbeButtonDn.Size = new System.Drawing.Size(114, 16);
            this.MeasOnProbeButtonDn.TabIndex = 6;
            this.MeasOnProbeButtonDn.Text = "MeasOnPButtonDn";
            this.MeasOnProbeButtonDn.UseVisualStyleBackColor = true;
            this.MeasOnProbeButtonDn.CheckedChanged += new System.EventHandler(this.ProbeButtonTrigger_Checked);
            // 
            // MeasStablecb
            // 
            this.MeasStablecb.AutoSize = true;
            this.MeasStablecb.Location = new System.Drawing.Point(13, 71);
            this.MeasStablecb.Name = "MeasStablecb";
            this.MeasStablecb.Size = new System.Drawing.Size(108, 16);
            this.MeasStablecb.TabIndex = 7;
            this.MeasStablecb.Text = "MeasWhenStable";
            this.MeasStablecb.UseVisualStyleBackColor = true;
            this.MeasStablecb.CheckedChanged += new System.EventHandler(this.StableTrigger_Checked);
            // 
            // Targetsgrp
            // 
            this.Targetsgrp.Controls.Add(this.splitContainer1);
            this.Targetsgrp.Location = new System.Drawing.Point(20, 20);
            this.Targetsgrp.Name = "Targetsgrp";
            this.Targetsgrp.Size = new System.Drawing.Size(771, 100);
            this.Targetsgrp.TabIndex = 0;
            this.Targetsgrp.TabStop = false;
            this.Targetsgrp.Text = "Targets";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 17);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.SelectedTarget);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.PreselectdTarget);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.ActiveTips);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.Targetscbox);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TargetQ3);
            this.splitContainer1.Panel2.Controls.Add(this.TargetQ2);
            this.splitContainer1.Panel2.Controls.Add(this.TargetQ1);
            this.splitContainer1.Panel2.Controls.Add(this.TargetQ0);
            this.splitContainer1.Panel2.Controls.Add(this.Targetposz);
            this.splitContainer1.Panel2.Controls.Add(this.Targetposy);
            this.splitContainer1.Panel2.Controls.Add(this.Targetposx);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Size = new System.Drawing.Size(765, 80);
            this.splitContainer1.SplitterDistance = 381;
            this.splitContainer1.TabIndex = 0;
            // 
            // SelectedTarget
            // 
            this.SelectedTarget.Location = new System.Drawing.Point(278, 45);
            this.SelectedTarget.Name = "SelectedTarget";
            this.SelectedTarget.Size = new System.Drawing.Size(95, 21);
            this.SelectedTarget.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(203, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "Selected:";
            // 
            // PreselectdTarget
            // 
            this.PreselectdTarget.Location = new System.Drawing.Point(278, 13);
            this.PreselectdTarget.Name = "PreselectdTarget";
            this.PreselectdTarget.Size = new System.Drawing.Size(95, 21);
            this.PreselectdTarget.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Preselected:";
            // 
            // ActiveTips
            // 
            this.ActiveTips.Location = new System.Drawing.Point(102, 45);
            this.ActiveTips.Name = "ActiveTips";
            this.ActiveTips.Size = new System.Drawing.Size(93, 21);
            this.ActiveTips.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "ActiveTips:";
            // 
            // Targetscbox
            // 
            this.Targetscbox.FormattingEnabled = true;
            this.Targetscbox.Location = new System.Drawing.Point(102, 14);
            this.Targetscbox.Name = "Targetscbox";
            this.Targetscbox.Size = new System.Drawing.Size(93, 20);
            this.Targetscbox.TabIndex = 1;
            this.Targetscbox.SelectionChangeCommitted += new System.EventHandler(this.SelectedTargetChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "UsableTargets:";
            // 
            // TargetQ3
            // 
            this.TargetQ3.Location = new System.Drawing.Point(284, 45);
            this.TargetQ3.Name = "TargetQ3";
            this.TargetQ3.Size = new System.Drawing.Size(80, 21);
            this.TargetQ3.TabIndex = 7;
            // 
            // TargetQ2
            // 
            this.TargetQ2.Location = new System.Drawing.Point(195, 45);
            this.TargetQ2.Name = "TargetQ2";
            this.TargetQ2.Size = new System.Drawing.Size(80, 21);
            this.TargetQ2.TabIndex = 6;
            // 
            // TargetQ1
            // 
            this.TargetQ1.Location = new System.Drawing.Point(106, 45);
            this.TargetQ1.Name = "TargetQ1";
            this.TargetQ1.Size = new System.Drawing.Size(80, 21);
            this.TargetQ1.TabIndex = 5;
            // 
            // TargetQ0
            // 
            this.TargetQ0.Location = new System.Drawing.Point(15, 45);
            this.TargetQ0.Name = "TargetQ0";
            this.TargetQ0.Size = new System.Drawing.Size(80, 21);
            this.TargetQ0.TabIndex = 4;
            // 
            // Targetposz
            // 
            this.Targetposz.Location = new System.Drawing.Point(284, 12);
            this.Targetposz.Name = "Targetposz";
            this.Targetposz.Size = new System.Drawing.Size(80, 21);
            this.Targetposz.TabIndex = 3;
            // 
            // Targetposy
            // 
            this.Targetposy.Location = new System.Drawing.Point(195, 12);
            this.Targetposy.Name = "Targetposy";
            this.Targetposy.Size = new System.Drawing.Size(80, 21);
            this.Targetposy.TabIndex = 2;
            // 
            // Targetposx
            // 
            this.Targetposx.Location = new System.Drawing.Point(106, 12);
            this.Targetposx.Name = "Targetposx";
            this.Targetposx.Size = new System.Drawing.Size(80, 21);
            this.Targetposx.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "TargetPos:";
            // 
            // Connecttab1
            // 
            this.Connecttab1.Controls.Add(this.panel1);
            this.Connecttab1.Location = new System.Drawing.Point(4, 4);
            this.Connecttab1.Name = "Connecttab1";
            this.Connecttab1.Padding = new System.Windows.Forms.Padding(3);
            this.Connecttab1.Size = new System.Drawing.Size(826, 476);
            this.Connecttab1.TabIndex = 0;
            this.Connecttab1.Text = "ConnectAndPositionto";
            this.Connecttab1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.ConnectedResults);
            this.panel1.Controls.Add(this.PositionToStationary);
            this.panel1.Controls.Add(this.PositionToGroup);
            this.panel1.Controls.Add(this.MoveControl);
            this.panel1.Controls.Add(this.Initialize_Disconnect);
            this.panel1.Controls.Add(this.Discover_tracker);
            this.panel1.Controls.Add(this.ConnectSim);
            this.panel1.Controls.Add(this.Trackers_list);
            this.panel1.Controls.Add(this.ConnectIP);
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(808, 460);
            this.panel1.TabIndex = 0;
            // 
            // ConnectedResults
            // 
            this.ConnectedResults.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ConnectedResults.Location = new System.Drawing.Point(20, 417);
            this.ConnectedResults.Multiline = true;
            this.ConnectedResults.Name = "ConnectedResults";
            this.ConnectedResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ConnectedResults.Size = new System.Drawing.Size(516, 36);
            this.ConnectedResults.TabIndex = 8;
            this.ConnectedResults.Text = "No Tracker Connected!";
            this.ConnectedResults.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PositionToStationary
            // 
            this.PositionToStationary.Controls.Add(this.PositionTarget);
            this.PositionToStationary.Controls.Add(this.PtchBox);
            this.PositionToStationary.Controls.Add(this.GoMeasurebt);
            this.PositionToStationary.Controls.Add(this.LockOnTokencBox);
            this.PositionToStationary.Controls.Add(this.LockonToken);
            this.PositionToStationary.Controls.Add(this.tableLayoutPanel3);
            this.PositionToStationary.Location = new System.Drawing.Point(547, 237);
            this.PositionToStationary.Name = "PositionToStationary";
            this.PositionToStationary.Size = new System.Drawing.Size(245, 216);
            this.PositionToStationary.TabIndex = 7;
            this.PositionToStationary.TabStop = false;
            this.PositionToStationary.Text = "Position to Target";
            // 
            // PositionTarget
            // 
            this.PositionTarget.Location = new System.Drawing.Point(113, 143);
            this.PositionTarget.Name = "PositionTarget";
            this.PositionTarget.Size = new System.Drawing.Size(111, 25);
            this.PositionTarget.TabIndex = 14;
            this.PositionTarget.Text = "PositiontoTarget";
            this.PositionTarget.UseVisualStyleBackColor = true;
            this.PositionTarget.Click += new System.EventHandler(this.PositiontoTarget_Click);
            // 
            // PtchBox
            // 
            this.PtchBox.AutoSize = true;
            this.PtchBox.Location = new System.Drawing.Point(18, 149);
            this.PtchBox.Name = "PtchBox";
            this.PtchBox.Size = new System.Drawing.Size(84, 16);
            this.PtchBox.TabIndex = 13;
            this.PtchBox.Text = "IsRelative";
            this.PtchBox.UseVisualStyleBackColor = true;
            // 
            // GoMeasurebt
            // 
            this.GoMeasurebt.Location = new System.Drawing.Point(113, 180);
            this.GoMeasurebt.Name = "GoMeasurebt";
            this.GoMeasurebt.Size = new System.Drawing.Size(111, 25);
            this.GoMeasurebt.TabIndex = 12;
            this.GoMeasurebt.Text = "Go And Measure";
            this.GoMeasurebt.UseVisualStyleBackColor = true;
            this.GoMeasurebt.Click += new System.EventHandler(this.GoMeasure_Click);
            // 
            // LockOnTokencBox
            // 
            this.LockOnTokencBox.FormattingEnabled = true;
            this.LockOnTokencBox.Location = new System.Drawing.Point(94, 113);
            this.LockOnTokencBox.Name = "LockOnTokencBox";
            this.LockOnTokencBox.Size = new System.Drawing.Size(130, 20);
            this.LockOnTokencBox.TabIndex = 11;
            // 
            // LockonToken
            // 
            this.LockonToken.AutoSize = true;
            this.LockonToken.Location = new System.Drawing.Point(16, 115);
            this.LockonToken.Name = "LockonToken";
            this.LockonToken.Size = new System.Drawing.Size(77, 12);
            this.LockonToken.TabIndex = 10;
            this.LockonToken.Text = "LockOnToken:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel3.Controls.Add(this.Xptlabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Yptlabel, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Zptlabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.Xptpos, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.Yptpos, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.Zptpos, 1, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(19, 25);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(205, 75);
            this.tableLayoutPanel3.TabIndex = 9;
            // 
            // Xptlabel
            // 
            this.Xptlabel.AutoSize = true;
            this.Xptlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Xptlabel.Location = new System.Drawing.Point(3, 0);
            this.Xptlabel.Name = "Xptlabel";
            this.Xptlabel.Size = new System.Drawing.Size(45, 25);
            this.Xptlabel.TabIndex = 0;
            this.Xptlabel.Text = "X:";
            this.Xptlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Yptlabel
            // 
            this.Yptlabel.AutoSize = true;
            this.Yptlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Yptlabel.Location = new System.Drawing.Point(3, 25);
            this.Yptlabel.Name = "Yptlabel";
            this.Yptlabel.Size = new System.Drawing.Size(45, 25);
            this.Yptlabel.TabIndex = 1;
            this.Yptlabel.Text = "Y:";
            this.Yptlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Zptlabel
            // 
            this.Zptlabel.AutoSize = true;
            this.Zptlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Zptlabel.Location = new System.Drawing.Point(3, 50);
            this.Zptlabel.Name = "Zptlabel";
            this.Zptlabel.Size = new System.Drawing.Size(45, 25);
            this.Zptlabel.TabIndex = 2;
            this.Zptlabel.Text = "Z:";
            this.Zptlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Xptpos
            // 
            this.Xptpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Xptpos.Location = new System.Drawing.Point(54, 3);
            this.Xptpos.Name = "Xptpos";
            this.Xptpos.Size = new System.Drawing.Size(148, 21);
            this.Xptpos.TabIndex = 3;
            // 
            // Yptpos
            // 
            this.Yptpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Yptpos.Location = new System.Drawing.Point(54, 28);
            this.Yptpos.Name = "Yptpos";
            this.Yptpos.Size = new System.Drawing.Size(148, 21);
            this.Yptpos.TabIndex = 4;
            // 
            // Zptpos
            // 
            this.Zptpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Zptpos.Location = new System.Drawing.Point(54, 53);
            this.Zptpos.Name = "Zptpos";
            this.Zptpos.Size = new System.Drawing.Size(148, 21);
            this.Zptpos.TabIndex = 5;
            // 
            // PositionToGroup
            // 
            this.PositionToGroup.Controls.Add(this.Positionto);
            this.PositionToGroup.Controls.Add(this.Ansyncbt);
            this.PositionToGroup.Controls.Add(this.SearchTargetchBox);
            this.PositionToGroup.Controls.Add(this.IsRelativechBox);
            this.PositionToGroup.Controls.Add(this.tableLayoutPanel2);
            this.PositionToGroup.Location = new System.Drawing.Point(291, 237);
            this.PositionToGroup.Name = "PositionToGroup";
            this.PositionToGroup.Size = new System.Drawing.Size(245, 174);
            this.PositionToGroup.TabIndex = 6;
            this.PositionToGroup.TabStop = false;
            this.PositionToGroup.Text = "Position to";
            // 
            // Positionto
            // 
            this.Positionto.Location = new System.Drawing.Point(18, 143);
            this.Positionto.Name = "Positionto";
            this.Positionto.Size = new System.Drawing.Size(80, 25);
            this.Positionto.TabIndex = 13;
            this.Positionto.Text = "PositionTo";
            this.Positionto.UseVisualStyleBackColor = true;
            this.Positionto.Click += new System.EventHandler(this.Positionto_Click);
            // 
            // Ansyncbt
            // 
            this.Ansyncbt.Location = new System.Drawing.Point(145, 143);
            this.Ansyncbt.Name = "Ansyncbt";
            this.Ansyncbt.Size = new System.Drawing.Size(80, 25);
            this.Ansyncbt.TabIndex = 12;
            this.Ansyncbt.Text = "Async";
            this.Ansyncbt.UseVisualStyleBackColor = true;
            this.Ansyncbt.Click += new System.EventHandler(this.PostoAnsync_Click);
            // 
            // SearchTargetchBox
            // 
            this.SearchTargetchBox.AutoSize = true;
            this.SearchTargetchBox.Location = new System.Drawing.Point(126, 115);
            this.SearchTargetchBox.Name = "SearchTargetchBox";
            this.SearchTargetchBox.Size = new System.Drawing.Size(96, 16);
            this.SearchTargetchBox.TabIndex = 10;
            this.SearchTargetchBox.Text = "SearchTarget";
            this.SearchTargetchBox.UseVisualStyleBackColor = true;
            // 
            // IsRelativechBox
            // 
            this.IsRelativechBox.AutoSize = true;
            this.IsRelativechBox.Location = new System.Drawing.Point(19, 115);
            this.IsRelativechBox.Name = "IsRelativechBox";
            this.IsRelativechBox.Size = new System.Drawing.Size(84, 16);
            this.IsRelativechBox.TabIndex = 9;
            this.IsRelativechBox.Text = "IsRelative";
            this.IsRelativechBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel2.Controls.Add(this.X_label, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Y_label, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.Z_label, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Xpos, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Ypos, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.Zpos, 1, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(20, 25);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(205, 75);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // X_label
            // 
            this.X_label.AutoSize = true;
            this.X_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.X_label.Location = new System.Drawing.Point(3, 0);
            this.X_label.Name = "X_label";
            this.X_label.Size = new System.Drawing.Size(45, 25);
            this.X_label.TabIndex = 0;
            this.X_label.Text = "X:";
            this.X_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Y_label
            // 
            this.Y_label.AutoSize = true;
            this.Y_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Y_label.Location = new System.Drawing.Point(3, 25);
            this.Y_label.Name = "Y_label";
            this.Y_label.Size = new System.Drawing.Size(45, 25);
            this.Y_label.TabIndex = 1;
            this.Y_label.Text = "Y:";
            this.Y_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Z_label
            // 
            this.Z_label.AutoSize = true;
            this.Z_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Z_label.Location = new System.Drawing.Point(3, 50);
            this.Z_label.Name = "Z_label";
            this.Z_label.Size = new System.Drawing.Size(45, 25);
            this.Z_label.TabIndex = 2;
            this.Z_label.Text = "Z:";
            this.Z_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Xpos
            // 
            this.Xpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Xpos.Location = new System.Drawing.Point(54, 3);
            this.Xpos.Name = "Xpos";
            this.Xpos.Size = new System.Drawing.Size(148, 21);
            this.Xpos.TabIndex = 3;
            // 
            // Ypos
            // 
            this.Ypos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Ypos.Location = new System.Drawing.Point(54, 28);
            this.Ypos.Name = "Ypos";
            this.Ypos.Size = new System.Drawing.Size(148, 21);
            this.Ypos.TabIndex = 4;
            // 
            // Zpos
            // 
            this.Zpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Zpos.Location = new System.Drawing.Point(54, 53);
            this.Zpos.Name = "Zpos";
            this.Zpos.Size = new System.Drawing.Size(148, 21);
            this.Zpos.TabIndex = 5;
            // 
            // MoveControl
            // 
            this.MoveControl.Controls.Add(this.GoBirdBathbt);
            this.MoveControl.Controls.Add(this.tableLayoutPanel1);
            this.MoveControl.Controls.Add(this.panel2);
            this.MoveControl.Location = new System.Drawing.Point(20, 237);
            this.MoveControl.Name = "MoveControl";
            this.MoveControl.Size = new System.Drawing.Size(260, 175);
            this.MoveControl.TabIndex = 5;
            this.MoveControl.TabStop = false;
            this.MoveControl.Text = "Move";
            // 
            // GoBirdBathbt
            // 
            this.GoBirdBathbt.Location = new System.Drawing.Point(20, 143);
            this.GoBirdBathbt.Name = "GoBirdBathbt";
            this.GoBirdBathbt.Size = new System.Drawing.Size(225, 25);
            this.GoBirdBathbt.TabIndex = 8;
            this.GoBirdBathbt.Text = "GoBirdBath(AT901)";
            this.GoBirdBathbt.UseVisualStyleBackColor = true;
            this.GoBirdBathbt.Click += new System.EventHandler(this.Gobirdbath_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.31446F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.68554F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel1.Controls.Add(this.MoveSpeed, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SpeedValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.SpeedUnit, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 105);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(225, 30);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // MoveSpeed
            // 
            this.MoveSpeed.AutoSize = true;
            this.MoveSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoveSpeed.Location = new System.Drawing.Point(3, 0);
            this.MoveSpeed.Name = "MoveSpeed";
            this.MoveSpeed.Size = new System.Drawing.Size(73, 30);
            this.MoveSpeed.TabIndex = 0;
            this.MoveSpeed.Text = "MoveSpeed:";
            this.MoveSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SpeedValue
            // 
            this.SpeedValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpeedValue.Location = new System.Drawing.Point(82, 3);
            this.SpeedValue.Name = "SpeedValue";
            this.SpeedValue.Size = new System.Drawing.Size(72, 21);
            this.SpeedValue.TabIndex = 1;
            this.SpeedValue.Text = "10";
            // 
            // SpeedUnit
            // 
            this.SpeedUnit.AutoSize = true;
            this.SpeedUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpeedUnit.Location = new System.Drawing.Point(160, 0);
            this.SpeedUnit.Name = "SpeedUnit";
            this.SpeedUnit.Size = new System.Drawing.Size(62, 30);
            this.SpeedUnit.TabIndex = 2;
            this.SpeedUnit.Text = "[u]";
            this.SpeedUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Rightbt);
            this.panel2.Controls.Add(this.Downbt);
            this.panel2.Controls.Add(this.Stopbt);
            this.panel2.Controls.Add(this.Leftbt);
            this.panel2.Controls.Add(this.Upbt);
            this.panel2.Location = new System.Drawing.Point(20, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(225, 75);
            this.panel2.TabIndex = 6;
            // 
            // Rightbt
            // 
            this.Rightbt.Location = new System.Drawing.Point(150, 25);
            this.Rightbt.Name = "Rightbt";
            this.Rightbt.Size = new System.Drawing.Size(75, 25);
            this.Rightbt.TabIndex = 4;
            this.Rightbt.Text = "Right";
            this.Rightbt.UseVisualStyleBackColor = true;
            this.Rightbt.Click += new System.EventHandler(this.Moveright_Click);
            // 
            // Downbt
            // 
            this.Downbt.Location = new System.Drawing.Point(0, 50);
            this.Downbt.Name = "Downbt";
            this.Downbt.Size = new System.Drawing.Size(225, 25);
            this.Downbt.TabIndex = 3;
            this.Downbt.Text = "Down";
            this.Downbt.UseVisualStyleBackColor = true;
            this.Downbt.Click += new System.EventHandler(this.Movedown_Click);
            // 
            // Stopbt
            // 
            this.Stopbt.Location = new System.Drawing.Point(75, 25);
            this.Stopbt.Name = "Stopbt";
            this.Stopbt.Size = new System.Drawing.Size(75, 25);
            this.Stopbt.TabIndex = 2;
            this.Stopbt.Text = "Stop";
            this.Stopbt.UseVisualStyleBackColor = true;
            this.Stopbt.Click += new System.EventHandler(this.Stopmove_Click);
            // 
            // Leftbt
            // 
            this.Leftbt.Location = new System.Drawing.Point(0, 25);
            this.Leftbt.Name = "Leftbt";
            this.Leftbt.Size = new System.Drawing.Size(75, 25);
            this.Leftbt.TabIndex = 1;
            this.Leftbt.Text = "Left";
            this.Leftbt.UseVisualStyleBackColor = true;
            this.Leftbt.Click += new System.EventHandler(this.Moveleft_Click);
            // 
            // Upbt
            // 
            this.Upbt.Location = new System.Drawing.Point(0, 0);
            this.Upbt.Name = "Upbt";
            this.Upbt.Size = new System.Drawing.Size(225, 25);
            this.Upbt.TabIndex = 0;
            this.Upbt.Text = "UP";
            this.Upbt.UseVisualStyleBackColor = true;
            this.Upbt.Click += new System.EventHandler(this.Moveup_Click);
            // 
            // Initialize_Disconnect
            // 
            this.Initialize_Disconnect.Controls.Add(this.Disconnectbt);
            this.Initialize_Disconnect.Controls.Add(this.Initializebt);
            this.Initialize_Disconnect.Location = new System.Drawing.Point(571, 20);
            this.Initialize_Disconnect.Name = "Initialize_Disconnect";
            this.Initialize_Disconnect.Size = new System.Drawing.Size(221, 65);
            this.Initialize_Disconnect.TabIndex = 4;
            this.Initialize_Disconnect.TabStop = false;
            this.Initialize_Disconnect.Text = "Initialize and Disconnect";
            // 
            // Disconnectbt
            // 
            this.Disconnectbt.Enabled = false;
            this.Disconnectbt.Location = new System.Drawing.Point(123, 21);
            this.Disconnectbt.Name = "Disconnectbt";
            this.Disconnectbt.Size = new System.Drawing.Size(75, 25);
            this.Disconnectbt.TabIndex = 1;
            this.Disconnectbt.Text = "Disconnect";
            this.Disconnectbt.UseVisualStyleBackColor = true;
            this.Disconnectbt.Click += new System.EventHandler(this.DisConnectAll_Click);
            // 
            // Initializebt
            // 
            this.Initializebt.Location = new System.Drawing.Point(22, 21);
            this.Initializebt.Name = "Initializebt";
            this.Initializebt.Size = new System.Drawing.Size(75, 25);
            this.Initializebt.TabIndex = 0;
            this.Initializebt.Text = "Initialize";
            this.Initializebt.UseVisualStyleBackColor = true;
            this.Initializebt.Click += new System.EventHandler(this.Initialize_Click);
            // 
            // Discover_tracker
            // 
            this.Discover_tracker.Controls.Add(this.ConnectDbt);
            this.Discover_tracker.Controls.Add(this.Discoverbt);
            this.Discover_tracker.Location = new System.Drawing.Point(20, 93);
            this.Discover_tracker.Name = "Discover_tracker";
            this.Discover_tracker.Size = new System.Drawing.Size(535, 65);
            this.Discover_tracker.TabIndex = 3;
            this.Discover_tracker.TabStop = false;
            this.Discover_tracker.Text = "Discover Trackers";
            // 
            // ConnectDbt
            // 
            this.ConnectDbt.Location = new System.Drawing.Point(428, 24);
            this.ConnectDbt.Name = "ConnectDbt";
            this.ConnectDbt.Size = new System.Drawing.Size(75, 25);
            this.ConnectDbt.TabIndex = 1;
            this.ConnectDbt.Text = "Connect";
            this.ConnectDbt.UseVisualStyleBackColor = true;
            this.ConnectDbt.Click += new System.EventHandler(this.ConnectDis_Clicked);
            // 
            // Discoverbt
            // 
            this.Discoverbt.Location = new System.Drawing.Point(22, 24);
            this.Discoverbt.Name = "Discoverbt";
            this.Discoverbt.Size = new System.Drawing.Size(157, 25);
            this.Discoverbt.TabIndex = 0;
            this.Discoverbt.Text = "DiscoverTrackers";
            this.Discoverbt.UseVisualStyleBackColor = true;
            this.Discoverbt.Click += new System.EventHandler(this.Discoverbt_Clicked);
            // 
            // ConnectSim
            // 
            this.ConnectSim.Controls.Add(this.AT960bt);
            this.ConnectSim.Controls.Add(this.AT930bt);
            this.ConnectSim.Controls.Add(this.AT901bt);
            this.ConnectSim.Controls.Add(this.AT403bt);
            this.ConnectSim.Controls.Add(this.AT402bt);
            this.ConnectSim.Controls.Add(this.AT401bt);
            this.ConnectSim.Location = new System.Drawing.Point(20, 165);
            this.ConnectSim.Name = "ConnectSim";
            this.ConnectSim.Size = new System.Drawing.Size(535, 65);
            this.ConnectSim.TabIndex = 2;
            this.ConnectSim.TabStop = false;
            this.ConnectSim.Text = "Connect to Simulator";
            // 
            // AT960bt
            // 
            this.AT960bt.Location = new System.Drawing.Point(428, 24);
            this.AT960bt.Name = "AT960bt";
            this.AT960bt.Size = new System.Drawing.Size(75, 25);
            this.AT960bt.TabIndex = 5;
            this.AT960bt.Tag = "at960simulator";
            this.AT960bt.Text = "AT960";
            this.AT960bt.UseVisualStyleBackColor = true;
            this.AT960bt.Click += new System.EventHandler(this.AT960Sim_Click);
            // 
            // AT930bt
            // 
            this.AT930bt.Location = new System.Drawing.Point(347, 24);
            this.AT930bt.Name = "AT930bt";
            this.AT930bt.Size = new System.Drawing.Size(75, 25);
            this.AT930bt.TabIndex = 4;
            this.AT930bt.Tag = "at930simulator";
            this.AT930bt.Text = "AT930";
            this.AT930bt.UseVisualStyleBackColor = true;
            this.AT930bt.Click += new System.EventHandler(this.AT930Sim_Click);
            // 
            // AT901bt
            // 
            this.AT901bt.Location = new System.Drawing.Point(266, 24);
            this.AT901bt.Name = "AT901bt";
            this.AT901bt.Size = new System.Drawing.Size(75, 25);
            this.AT901bt.TabIndex = 3;
            this.AT901bt.Tag = "at901simulator";
            this.AT901bt.Text = "AT901";
            this.AT901bt.UseVisualStyleBackColor = true;
            this.AT901bt.Click += new System.EventHandler(this.AT901Sim_Click);
            // 
            // AT403bt
            // 
            this.AT403bt.Location = new System.Drawing.Point(185, 24);
            this.AT403bt.Name = "AT403bt";
            this.AT403bt.Size = new System.Drawing.Size(75, 25);
            this.AT403bt.TabIndex = 2;
            this.AT403bt.Tag = "at403simulator";
            this.AT403bt.Text = "AT403";
            this.AT403bt.UseVisualStyleBackColor = true;
            this.AT403bt.Click += new System.EventHandler(this.AT403Sim_Click);
            // 
            // AT402bt
            // 
            this.AT402bt.Location = new System.Drawing.Point(104, 24);
            this.AT402bt.Name = "AT402bt";
            this.AT402bt.Size = new System.Drawing.Size(75, 25);
            this.AT402bt.TabIndex = 1;
            this.AT402bt.Tag = "at402simulator";
            this.AT402bt.Text = "AT402";
            this.AT402bt.UseVisualStyleBackColor = true;
            this.AT402bt.Click += new System.EventHandler(this.AT402Sim_Click);
            // 
            // AT401bt
            // 
            this.AT401bt.Location = new System.Drawing.Point(22, 24);
            this.AT401bt.Name = "AT401bt";
            this.AT401bt.Size = new System.Drawing.Size(75, 25);
            this.AT401bt.TabIndex = 0;
            this.AT401bt.Tag = "at401simulator";
            this.AT401bt.Text = "AT401";
            this.AT401bt.UseVisualStyleBackColor = true;
            this.AT401bt.Click += new System.EventHandler(this.AT401Sim_Click);
            // 
            // Trackers_list
            // 
            this.Trackers_list.FormattingEnabled = true;
            this.Trackers_list.ItemHeight = 12;
            this.Trackers_list.Location = new System.Drawing.Point(571, 103);
            this.Trackers_list.Name = "Trackers_list";
            this.Trackers_list.Size = new System.Drawing.Size(221, 124);
            this.Trackers_list.TabIndex = 1;
            // 
            // ConnectIP
            // 
            this.ConnectIP.Controls.Add(this.IPAddress);
            this.ConnectIP.Controls.Add(this.ConnectstaticIP);
            this.ConnectIP.Controls.Add(this.IPlabel);
            this.ConnectIP.Location = new System.Drawing.Point(20, 20);
            this.ConnectIP.Name = "ConnectIP";
            this.ConnectIP.Size = new System.Drawing.Size(535, 65);
            this.ConnectIP.TabIndex = 0;
            this.ConnectIP.TabStop = false;
            this.ConnectIP.Text = "Connect to IP";
            // 
            // IPAddress
            // 
            this.IPAddress.Location = new System.Drawing.Point(48, 25);
            this.IPAddress.Name = "IPAddress";
            this.IPAddress.Size = new System.Drawing.Size(163, 21);
            this.IPAddress.TabIndex = 2;
            this.IPAddress.Text = "192.168.0.1";
            this.IPAddress.TextChanged += new System.EventHandler(this.IpAddressChanged);
            // 
            // ConnectstaticIP
            // 
            this.ConnectstaticIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ConnectstaticIP.Location = new System.Drawing.Point(428, 20);
            this.ConnectstaticIP.Name = "ConnectstaticIP";
            this.ConnectstaticIP.Size = new System.Drawing.Size(75, 25);
            this.ConnectstaticIP.TabIndex = 1;
            this.ConnectstaticIP.Text = "Connect";
            this.ConnectstaticIP.UseVisualStyleBackColor = true;
            this.ConnectstaticIP.Click += new System.EventHandler(this.ConnectstaticIP_Click);
            // 
            // IPlabel
            // 
            this.IPlabel.AutoSize = true;
            this.IPlabel.Location = new System.Drawing.Point(20, 29);
            this.IPlabel.Name = "IPlabel";
            this.IPlabel.Size = new System.Drawing.Size(23, 12);
            this.IPlabel.TabIndex = 0;
            this.IPlabel.Text = "IP:";
            // 
            // Connecttabs
            // 
            this.Connecttabs.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.Connecttabs.Controls.Add(this.Connecttab1);
            this.Connecttabs.Controls.Add(this.Measuringtab2);
            this.Connecttabs.Controls.Add(this.TrackerErrortab3);
            this.Connecttabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Connecttabs.Location = new System.Drawing.Point(0, 25);
            this.Connecttabs.Multiline = true;
            this.Connecttabs.Name = "Connecttabs";
            this.Connecttabs.SelectedIndex = 0;
            this.Connecttabs.Size = new System.Drawing.Size(834, 502);
            this.Connecttabs.TabIndex = 1;
            // 
            // TrackerErrortab3
            // 
            this.TrackerErrortab3.AccessibleName = "";
            this.TrackerErrortab3.Controls.Add(this.Errorsgrp);
            this.TrackerErrortab3.Location = new System.Drawing.Point(4, 4);
            this.TrackerErrortab3.Name = "TrackerErrortab3";
            this.TrackerErrortab3.Padding = new System.Windows.Forms.Padding(3);
            this.TrackerErrortab3.Size = new System.Drawing.Size(826, 476);
            this.TrackerErrortab3.TabIndex = 3;
            this.TrackerErrortab3.Text = "TrackerErrors";
            this.TrackerErrortab3.UseVisualStyleBackColor = true;
            // 
            // Errorsgrp
            // 
            this.Errorsgrp.Controls.Add(this.ErrorDescriptiontb);
            this.Errorsgrp.Controls.Add(this.Descriptionbt);
            this.Errorsgrp.Controls.Add(this.ErrorNumbertb);
            this.Errorsgrp.Location = new System.Drawing.Point(30, 30);
            this.Errorsgrp.Name = "Errorsgrp";
            this.Errorsgrp.Size = new System.Drawing.Size(760, 285);
            this.Errorsgrp.TabIndex = 0;
            this.Errorsgrp.TabStop = false;
            this.Errorsgrp.Text = "Error Description";
            // 
            // ErrorDescriptiontb
            // 
            this.ErrorDescriptiontb.Location = new System.Drawing.Point(15, 66);
            this.ErrorDescriptiontb.Multiline = true;
            this.ErrorDescriptiontb.Name = "ErrorDescriptiontb";
            this.ErrorDescriptiontb.Size = new System.Drawing.Size(727, 199);
            this.ErrorDescriptiontb.TabIndex = 2;
            // 
            // Descriptionbt
            // 
            this.Descriptionbt.Location = new System.Drawing.Point(143, 27);
            this.Descriptionbt.Name = "Descriptionbt";
            this.Descriptionbt.Size = new System.Drawing.Size(187, 23);
            this.Descriptionbt.TabIndex = 1;
            this.Descriptionbt.Text = "Get Description";
            this.Descriptionbt.UseVisualStyleBackColor = true;
            this.Descriptionbt.Click += new System.EventHandler(this.GetDescription_Click);
            // 
            // ErrorNumbertb
            // 
            this.ErrorNumbertb.Location = new System.Drawing.Point(15, 29);
            this.ErrorNumbertb.Name = "ErrorNumbertb";
            this.ErrorNumbertb.Size = new System.Drawing.Size(122, 21);
            this.ErrorNumbertb.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 527);
            this.Controls.Add(this.Connecttabs);
            this.Controls.Add(this.Menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "LaserTrackerController";
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.Measuringtab2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.Measurementgrp.ResumeLayout(false);
            this.MeasureSettings.ResumeLayout(false);
            this.MeasureSettingsgrp.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.Triggersgrp.ResumeLayout(false);
            this.Triggersgrp.PerformLayout();
            this.Targetsgrp.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.Connecttab1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PositionToStationary.ResumeLayout(false);
            this.PositionToStationary.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.PositionToGroup.ResumeLayout(false);
            this.PositionToGroup.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.MoveControl.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.Initialize_Disconnect.ResumeLayout(false);
            this.Discover_tracker.ResumeLayout(false);
            this.ConnectSim.ResumeLayout(false);
            this.ConnectIP.ResumeLayout(false);
            this.ConnectIP.PerformLayout();
            this.Connecttabs.ResumeLayout(false);
            this.TrackerErrortab3.ResumeLayout(false);
            this.Errorsgrp.ResumeLayout(false);
            this.Errorsgrp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem measureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cartesianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sphericalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cylindricalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quartersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rPYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationAnglesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oVCToolStripMenuItem;
        private System.Windows.Forms.TabPage Measuringtab2;
        private System.Windows.Forms.TabPage Connecttab1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox PositionToStationary;
        private System.Windows.Forms.CheckBox PtchBox;
        private System.Windows.Forms.Button GoMeasurebt;
        private System.Windows.Forms.ComboBox LockOnTokencBox;
        private System.Windows.Forms.Label LockonToken;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label Xptlabel;
        private System.Windows.Forms.Label Yptlabel;
        private System.Windows.Forms.Label Zptlabel;
        private System.Windows.Forms.TextBox Xptpos;
        private System.Windows.Forms.TextBox Yptpos;
        private System.Windows.Forms.TextBox Zptpos;
        private System.Windows.Forms.GroupBox PositionToGroup;
        private System.Windows.Forms.Button Ansyncbt;
        private System.Windows.Forms.CheckBox SearchTargetchBox;
        private System.Windows.Forms.CheckBox IsRelativechBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label X_label;
        private System.Windows.Forms.Label Y_label;
        private System.Windows.Forms.Label Z_label;
        private System.Windows.Forms.TextBox Xpos;
        private System.Windows.Forms.TextBox Ypos;
        private System.Windows.Forms.TextBox Zpos;
        private System.Windows.Forms.GroupBox MoveControl;
        private System.Windows.Forms.Button GoBirdBathbt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label MoveSpeed;
        private System.Windows.Forms.TextBox SpeedValue;
        private System.Windows.Forms.Label SpeedUnit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Rightbt;
        private System.Windows.Forms.Button Downbt;
        private System.Windows.Forms.Button Stopbt;
        private System.Windows.Forms.Button Leftbt;
        private System.Windows.Forms.Button Upbt;
        private System.Windows.Forms.GroupBox Initialize_Disconnect;
        private System.Windows.Forms.Button Disconnectbt;
        private System.Windows.Forms.Button Initializebt;
        private System.Windows.Forms.GroupBox Discover_tracker;
        private System.Windows.Forms.Button ConnectDbt;
        private System.Windows.Forms.Button Discoverbt;
        private System.Windows.Forms.GroupBox ConnectSim;
        private System.Windows.Forms.Button AT960bt;
        private System.Windows.Forms.Button AT930bt;
        private System.Windows.Forms.Button AT901bt;
        private System.Windows.Forms.Button AT403bt;
        private System.Windows.Forms.Button AT402bt;
        private System.Windows.Forms.Button AT401bt;
        private System.Windows.Forms.ListBox Trackers_list;
        private System.Windows.Forms.GroupBox ConnectIP;
        private System.Windows.Forms.TextBox IPAddress;
        private System.Windows.Forms.Button ConnectstaticIP;
        private System.Windows.Forms.Label IPlabel;
        private System.Windows.Forms.TabControl Connecttabs;
        private System.Windows.Forms.Button Positionto;
        private System.Windows.Forms.Button PositionTarget;
        private System.Windows.Forms.TextBox ConnectedResults;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox Targetsgrp;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Targetscbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SelectedTarget;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox PreselectdTarget;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ActiveTips;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Targetposx;
        private System.Windows.Forms.TextBox Targetposz;
        private System.Windows.Forms.TextBox Targetposy;
        private System.Windows.Forms.TextBox TargetQ3;
        private System.Windows.Forms.TextBox TargetQ2;
        private System.Windows.Forms.TextBox TargetQ1;
        private System.Windows.Forms.TextBox TargetQ0;
        private System.Windows.Forms.GroupBox MeasureSettings;
        private System.Windows.Forms.CheckBox MeasStablecb;
        private System.Windows.Forms.CheckBox MeasOnProbeButtonDn;
        private System.Windows.Forms.GroupBox MeasureSettingsgrp;
        private System.Windows.Forms.ListBox Preconditionlst;
        private System.Windows.Forms.GroupBox Measurementgrp;
        private System.Windows.Forms.ListBox MeasurementResultslst;
        private System.Windows.Forms.Button StopMeasurebt;
        private System.Windows.Forms.Button MeasureStatSyncbt;
        private System.Windows.Forms.Button Startmeasurebt;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox Profilescmb;
        private System.Windows.Forms.Label Profileslb;
        private System.Windows.Forms.RadioButton Fastbt;
        private System.Windows.Forms.TextBox ContDisttxt;
        private System.Windows.Forms.TextBox ContTimetxt;
        private System.Windows.Forms.Label ContTimePro;
        private System.Windows.Forms.Label ContDistPro;
        private System.Windows.Forms.Label StationaryPro;
        private System.Windows.Forms.RadioButton Precise;
        private System.Windows.Forms.RadioButton Standard;
        private System.Windows.Forms.ToolStripMenuItem lengthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem angleUnitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem degreeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gonToolStripMenuItem;
        private System.Windows.Forms.GroupBox Triggersgrp;
        private System.Windows.Forms.CheckBox RemoteControlcb;
        private System.Windows.Forms.Button WriteToExcelbt;
        private System.Windows.Forms.ToolStripMenuItem oVCToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage TrackerErrortab3;
        private System.Windows.Forms.ToolStripMenuItem alignmentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.GroupBox Errorsgrp;
        private System.Windows.Forms.TextBox ErrorNumbertb;
        private System.Windows.Forms.TextBox ErrorDescriptiontb;
        private System.Windows.Forms.Button Descriptionbt;
    }
}

