﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaserTrackerController.Models;
using LMF.Tracker;
using LMF.Tracker.Enums;
using LMF.Tracker.ErrorHandling;
using LMF.Tracker.Trackers;
using LMF.Tracker.Targets;
using LMF.Tracker.MeasurementResults;
using LMF.Tracker.Measurements;
using LMF.Tracker.Measurements.Profiles;
using LMF.Tracker.Targets.Probes.ActiveProbes;
using LMF.Tracker.Targets.Reflectors;
using LMF.Tracker.Triggers;

namespace LaserTrackerController
{
    public partial class MainForm : Form
    {
        private string _ipAddress;
        private readonly ConnectPageModel _connectPageModel;
        private string _moveSpeed;
        private readonly PositionModel _positionModel;
        private double _postarx;
        private double _postary;
        private double _postarz;
        private bool _isRelative;
        private LockOnToken _postarLockOnToken;
        private readonly MeasurenModel _measurenModel;
        private readonly MenuModel _menuModel;
        private TrackerErrorModel _trackerErrorModel;


        public MainForm()
        {
            InitializeComponent();
            _connectPageModel=new ConnectPageModel();
            RuntimeGlobals.Connection=new Connection();
            _ipAddress = IPAddress.Text;
            _positionModel=new PositionModel();
            _measurenModel=new MeasurenModel();
            _menuModel=new MenuModel();
            _trackerErrorModel=new TrackerErrorModel();
        }

        private void SetModels()
        {
            _connectPageModel.SetConnectModel();
            _positionModel.SetPositionModes();
            //获取LockOnToken并加入列表
            _positionModel.GetLockOnToken();//类一定义就强行获取
            LockOnTokencBox.Items.Clear();
            LockOnTokencBox.Items.AddRange(_positionModel.LockOnTokenList.Select(t => t.Name).ToArray<object>());
            //设置MeasureModel
            _measurenModel.SetModelEvents();
            _menuModel.SetBasicModels();
            _trackerErrorModel.SetTrackerErrorModel();
        }
        //
        private void ClearAllModes()
        {
            _connectPageModel.ClearConnectModel();
            ClearPositionToModes();
            _positionModel.ClearPositionModes();
            _measurenModel.ClearMeasureModes();
            _trackerErrorModel.ClearTrackerErrorModel();
            _menuModel.ClearMenuModes();
        }
        //清空PositionTo页面的相关内容
        private void ClearPositionToModes()
        {
            IPAddress.Text = "192.168.0.1";
            SpeedValue.Text = "10";
            Xpos.Text = null;
            Ypos.Text = null;
            Zpos.Text = null;
            IsRelativechBox.Checked = false;
            SearchTargetchBox.Checked = false;
            Xptpos.Text = null;
            Yptpos.Text = null;
            Zptpos.Text = null;
            LockOnTokencBox.Items.Clear();
            PtchBox.Checked = false;
        }
        //清空MeasureModes页面的相关内容
        public void ClearMeasureModes()
        {
            MeasOnProbeButtonDn.Checked = false;
            MeasStablecb.Checked = false;
            RemoteControlcb.Checked = false;
            Preconditionlst.Items.Clear();
            Profilescmb.Items.Clear();
            Fastbt.Checked = false;
            Standard.Checked = false;
            Precise.Checked = false;
            ContDisttxt.Text = null;
            ContTimetxt.Text = null;
            MeasurementResultslst.Items.Clear();
            WriteToExcelbt.Enabled = false;
        }

        public void SetTargetList(List<Target> targets)
        {
            Targetscbox.Items.Clear();
            Targetscbox.Text = null;
            if (targets!=null)
            {
                Targetscbox.Text = targets[0].ToString();
                Targetscbox.Items.AddRange(targets.Select(t => t.ToString()).ToArray<object>());//不知道为什么必须加object
            }
        }

        //****************************Connect**********************************
        void FinishConnect()
        {
            //连接到Tracker后设置模式，但是有一点，如果刚刚连接Tracker，还未连接到目标点？？？？
            SetModels();
        }

        void ConnecttoTracker(string trackerip)
        {
            //连接前检查是否有任务正在执行
            if (AsyncHelper.HasRunningTasks()) return;
            //连接前检查是否已有连接的Tracker；清空所有设置；
            //if (_connectPageModel.DisconnectIfConnected())
            //{
            //    ClearAllModes();
            //}
            if (_connectPageModel.IsTrackerConnected())
            {
                ClearAllModes();
                _connectPageModel.Disconnect();
            }
            if (string.IsNullOrEmpty(trackerip)) return; //检查IP
            AsyncHelper.ExecuteAsync(()=>_connectPageModel.Connect(trackerip),(ex) =>
                {
                    if (ex == null)
                    {
                        FinishConnect();
                        //Connecttabs.SelectTab(1);  //貌似是自动跳转页面的，我们不用
                        Disconnectbt.Enabled = true;
                    }
                });


        }

        private void ConnectstaticIP_Click(object sender, EventArgs e)
        {
            /* 自己的代码
            try
            {
                _ipAddress = IPAddress.Text;
                var tracker = new Connection().Connect(_ipAddress);
            }
            catch (Exception)
            {
                return;
            }
            */

            if (string.IsNullOrEmpty(_ipAddress)) return;
            ConnecttoTracker(_ipAddress);
        }

        private void Discoverbt_Clicked(object sender, EventArgs e)
        {
            AsyncHelper.ExecuteAsync(_connectPageModel.Discover);
        }

        public void SetDiscoveredTrackers(string[] trackers)
        {
            Invoke(new Action(() =>
            {
                Trackers_list.Items.Clear();
                if(trackers==null||!trackers.Any()) return;
                foreach (var tracker in trackers)
                {
                    Trackers_list.Items.Add(tracker);
                }
            }));
        }

        private void ConnectDis_Clicked(object sender, EventArgs e)
        {

            //源程序：格式匹配来进行连接   //var ip_str = "{aaa.bbb.c.d}";
            //var connectip = Regex.Match(Trackers_list.SelectedItem.ToString(), "[0-9.]+[0-9.]+[0-9.]+[0-9.]+");
            //另一种思路
            if (Trackers_list.SelectedItem == null)
            {
                MessageBox.Show("Please choose a Tracker first!");
                return;
            }
            string tracker_ip = Trackers_list.SelectedItem.ToString().Substring(0, 11);
            if (string.IsNullOrEmpty(tracker_ip)) return;
            ConnecttoTracker(tracker_ip);
        }

        //连接仿真器
        private void ConnectSimulator(object sender)
        {
            string trackerSimulator = ((Button)sender).Tag.ToString();
            ConnecttoTracker(trackerSimulator);
        }
        
        private void AT401Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }
        private void AT402Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }

        private void AT403Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }

        private void AT901Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }

        private void AT930Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }

        private void AT960Sim_Click(object sender, EventArgs e)
        {
            ConnectSimulator(sender);
        }
        //断开连接
        private void DisConnectAll_Click(object sender, EventArgs e)
        {
            if (((Button) sender).Enabled)
            {
                //if (_connectPageModel.DisconnectIfConnected())
                //{
                //    ClearAllModes();
                //    ((Button)sender).Enabled = false;
                //}
                if (_connectPageModel.IsTrackerConnected())
                {
                    ClearAllModes();
                    _connectPageModel.Disconnect();
                    ((Button)sender).Enabled = false;
                }
            }
        }

        private void Initialize_Click(object sender, EventArgs e)
        {
            if ((_connectPageModel.IsTrackerConnected()) && (!AsyncHelper.HasRunningTasks()))
            {
                AsyncHelper.ExecuteAsync(_connectPageModel.InitializeTracker);
            }
        }

        // 显示连接情况及颜色变化
        public void ShowConnectedResults(string situation,string color)
        {
            Invoke(new Action(() =>
            {
                ConnectedResults.Text = situation;
                switch (color)
                {
                    case "Red":
                        ConnectedResults.BackColor = Color.Red;
                        break;
                    case "Yellow": ConnectedResults.BackColor = Color.Yellow;
                        break;
                    case "White": ConnectedResults.BackColor = Color.White;break;
                        default: break;
                }

            }));
        }

        //**********************************Move************************************
        //限制输入的速度值为整型数
        private void SpeedtoInt()
        {
            try
            {
                _moveSpeed = SpeedValue.Text;
                double speed = double.Parse(_moveSpeed);
                int realspeed = (int) speed;
                _moveSpeed = realspeed.ToString();
                SpeedValue.Text = _moveSpeed;
            }
            catch
            {
                Console.WriteLine("Wrong in SpeedtoInt: Input speed is not a number");
            }

        }
        //运动控制
        private void Moveup_Click(object sender, EventArgs e)
        {
            SpeedtoInt();

            if (_connectPageModel.IsTrackerConnected())
            {
                _positionModel.Move(_moveSpeed,"up");
            }
        }

        private void Moveleft_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                SpeedtoInt();
                _positionModel.Move(_moveSpeed, "left");
            }
        }

        private void Moveright_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                SpeedtoInt();
                _positionModel.Move(_moveSpeed, "right");
            }
        }

        private void Movedown_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                SpeedtoInt();
                _positionModel.Move(_moveSpeed, "down");
            }
        }

        private void Stopmove_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _positionModel.Move("1", "stop");
            }
            /* 源码
            if (_connectPageModel.IsTrackerConnected() && !AsyncHelper.HasRunningTasks())
            {
                _positionModel.Move("0", "stop");
            }*/
        }
        // Gobirdbath命令
        private void Gobirdbath_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _positionModel.GoBirdBath();
        }

        //******************************Positionto*****************************
        private void PositiontoSpe(bool isAsync)
        {
            double posx = double.Parse(Xpos.Text);
            double posy = double.Parse(Ypos.Text);
            double posz = double.Parse(Zpos.Text);

            bool isRelative = IsRelativechBox.Checked;
            bool searchTar = SearchTargetchBox.Checked;

            _positionModel.PositionToSpecific(searchTar, isRelative, posx, posy, posz, isAsync);
        }

        private void Positionto_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected() && !AsyncHelper.HasRunningTasks())
            {
                AsyncHelper.ExecuteAsync(() =>
                {
                    PositiontoSpe(false);
                });
            }
        }

        private void PostoAnsync_Click(object sender, EventArgs e)
        {
            //调用异步定位方法就会自动开辟一个线程
            if (_connectPageModel.IsTrackerConnected() && !AsyncHelper.HasRunningTasks())
            {
                AsyncHelper.ExecuteAsync(() =>
                {
                    PositiontoSpe(true);
                });
            }
        }

        //*************************Position to Target************************
        private void PosTarSet()
        {
            _postarx = double.Parse(Xptpos.Text);
            _postary = double.Parse(Yptpos.Text);
            _postarz = double.Parse(Zptpos.Text);
            _isRelative = PtchBox.Checked;
            _postarLockOnToken = _positionModel.LockOnTokenList[LockOnTokencBox.SelectedIndex];
        }

        private void PositiontoTarget_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                try
                {
                    PosTarSet();
                    Target pos_target = _positionModel.PositionToTarget(_postarLockOnToken, _isRelative, _postarx,
                        _postary, _postarz);

                    if (pos_target != null)
                        ConnectedResults.Text += Environment.NewLine+"Detected Results: " + pos_target.ToString();
                    else
                        ConnectedResults.Text = Environment.NewLine + "No results.";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wrong in PositiontoTarget:\n" + ex.Message);
                    //throw;
                }
                   
            }
        }

        private void GoMeasure_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                try
                {
                    PosTarSet();
                    Measurement goMeasureResult = _positionModel.GoandMeasure(_postarLockOnToken, _isRelative, _postarx,
                        _postary, _postarz);
                    if (goMeasureResult != null)
                        ConnectedResults.Text += Environment.NewLine+"GoAndMeasure results: " + goMeasureResult.ToString();
                    else
                        ConnectedResults.Text += Environment.NewLine + "No GoAndMeasure Results!";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wrong in PositiontoTarget:\n" + ex.Message);
                    //throw;
                }
                    
            }
        }

        //*****************************Measure********************************
        //更新预选Target列表
        public void UpdatePreselectTarget(Target tar)
        {
            PreselectdTarget.Text = null;
            if (tar!=null)
                PreselectdTarget.Text = tar.ToString();
        }
        //更新选中的Target列表
        public void UpdateSelectedTarget(Target tar)
        {
            SelectedTarget.Text = null;
            if (tar!=null)
                SelectedTarget.Text = tar.ToString();
        }
        //当Target列表中的选定项变化时，手动更改Tracker的Target
        private void SelectedTargetChanged(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                TargetCollection targets = RuntimeGlobals.LMFTracker.Targets;
                foreach (Target targetGoal in targets)
                {
                    if (Targetscbox.SelectedItem.ToString().Equals(targetGoal.ToString()))
                    {
                        if (targetGoal is Reflector)
                        {
                            targetGoal.Select();
                        }
                    }
                }
            }
        }
        //显示ActiveProbe对应的ActiveTip
        public void ShowActiveTips()
        {
            if (!_connectPageModel.IsTrackerConnected())
            {
                ActiveTips.Text = null;
                return;
            }
            Target selectedTarget = RuntimeGlobals.LMFTracker.Targets.Selected;
            if (selectedTarget is ActiveProbe)
            {
                var tar = selectedTarget as ActiveProbe;
                var tipcollection = tar.Tips;
                //if (tipcollection.Count != 0)
                //    ActiveTips.Text = tar.Tips.Selected.ToString();
                if (tipcollection.Selected != null)
                    ActiveTips.Text = tar.Tips.Selected.ToString();
            }
        }

        public void ClearTips()
        {
            ActiveTips.Text = null;
        }

        //输出目标点位姿信息

        public void ShowPos(string x, string y, string z, string q0 = null, string q1 = null, string q2 = null, string q3 = null)
        {
            Targetposx.Text = null;
            Targetposy.Text = null;
            Targetposz.Text = null;
            TargetQ0.Text = null;
            TargetQ1.Text = null;
            TargetQ2.Text = null;
            TargetQ3.Text = null;

            if (x == null || y == null || z == null) return;

            string s1 = "X: ";
            string s2 = "Y: ";
            string s3 = "Z: ";

            Targetposx.Text = s1 + x + RuntimeGlobals.LengthUnit;
            Targetposy.Text = s2 + y + RuntimeGlobals.LengthUnit;
            Targetposz.Text = s3 + z + RuntimeGlobals.LengthUnit;

            if (!(q0 == null || q1 == null || q2 == null || q3 == null))
            {
                string s4 = "Q0: ";
                string s5 = "Q1: ";
                string s6 = "Q2: ";
                string s7 = "Q3: ";

                TargetQ0.Text = s4 + q0 + RuntimeGlobals.AngleUnit;
                TargetQ1.Text = s5 + q1 + RuntimeGlobals.AngleUnit;
                TargetQ2.Text = s6 + q2 + RuntimeGlobals.AngleUnit;
                TargetQ3.Text = s7 + q3 + RuntimeGlobals.AngleUnit;
            }
        }

        //测量之前需要设置好Preconditions,所以需要将其输出以便设置
        public void ShowPreconditions(string[] preconditions)
        {
            Invoke(new Action(() =>
            {
                Preconditionlst.Items.Clear();
                if (preconditions != null && preconditions.Any())
                {
                    foreach (var precondition in preconditions)
                    {
                        Preconditionlst.Items.Add(precondition);
                    }
                }
            }));
            /*
            Preconditionlst.Items.Clear();            
            if (preconditions != null && preconditions.Any())
            {
                foreach (var precondition in preconditions)
                {
                    Preconditionlst.Items.Add(precondition);
                }
            }
            */
        }
        //设置Profiles
        public void ShowProfiles(MeasurementProfile measprofile, string[] profiles)
        {
            Invoke(new Action(() =>
            {
                Profilescmb.Items.Clear();
                if (measprofile != null)
                {
                    Profilescmb.Text = measprofile.Name;
                    if (measprofile is StationaryMeasurementProfile profile)
                    {
                        switch (profile.Accuracy.Value.ToString())
                        {
                            case "Precise":
                                Precise.Enabled = true;
                                Precise.Checked = true;
                                break;
                            case "Standard":
                                Standard.Enabled = true;
                                Standard.Checked = true;
                                break;
                            case "Fast":
                                Fastbt.Enabled = true;
                                Fastbt.Checked = true;
                                break;
                        }
                    }
                    if (measprofile is ContinuousTimeProfile timeProfile)
                    {
                        ContTimetxt.Enabled = true;
                        ContTimetxt.Text = timeProfile.TimeSeparation.Value.ToString();
                    }

                    if (measprofile is ContinuousDistanceProfile)
                    {
                        ContDisttxt.Enabled = true;
                        ContDisttxt.Text =
                            ((ContinuousDistanceProfile) measprofile).DistanceSeparation.Value.ToString();
                    }
                }
                foreach (var profile in profiles)
                {
                    Profilescmb.Items.Add(profile);
                }
            }));
        }
        //设置Profiles对应的选定项
        private void ProfileSelected_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                try
                {
                    if (Profilescmb.Items.Count > 0)
                    {
                        RuntimeGlobals.LMFTracker.Measurement.Profiles[Profilescmb.SelectedIndex].Select();
                        switch (Profilescmb.SelectedItem.ToString())
                        {
                            case "Stationary":
                                Fastbt.Enabled = true;
                                Standard.Enabled = true;
                                Precise.Enabled = true;
                                ContDisttxt.Enabled = false;
                                ContTimetxt.Enabled = false;
                                break;

                            case "Continuous Time":
                                Fastbt.Enabled = false;
                                Standard.Enabled = false;
                                Precise.Enabled = false;
                                ContDisttxt.Enabled = false;
                                ContTimetxt.Enabled = true;
                                break;

                            case "Continuous Distance":
                                Fastbt.Enabled = false;
                                Standard.Enabled = false;
                                Precise.Enabled = false;
                                ContDisttxt.Enabled = true;
                                ContTimetxt.Enabled = false;
                                break;
                            default: break;
                        }
                    }
                }
                catch
                {
                    //Write Something?
                }
            }
        }
        //设置静态测量精度Fast
        private void Accuracy_Fast(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                if (!(RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected is StationaryMeasurementProfile)) return;
                if (Fastbt.Checked)
                {
                    ((StationaryMeasurementProfile)RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected)
                        .Accuracy.Value = EAccuracy.Fast;
                }
            }
        }
        //设置静态测量精度Standard
        private void Accuracy_Standard(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                if (!(RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected is StationaryMeasurementProfile)) return;
                if (Standard.Checked)
                {
                    ((StationaryMeasurementProfile)RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected)
                        .Accuracy.Value = EAccuracy.Standard;
                }
            }
        }
        //设置静态测量精度Precise
        private void Accuracy_Precise(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                if (!(RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected is StationaryMeasurementProfile)) return;
                if (Precise.Checked)
                {
                    ((StationaryMeasurementProfile)RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected)
                        .Accuracy.Value = EAccuracy.Precise;
                }
            }
        }
        //设置连续距离测量中的距离间隔
        private void ContinuousDis(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                if (!(RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected is ContinuousDistanceProfile)) return;
                try
                {
                    ((ContinuousDistanceProfile)RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected)
                        .DistanceSeparation.Value = double.Parse(ContDisttxt.Text);
                }
                catch
                {
                    //MessageBox.Show("Please Input a Number!");
                }
            }           
        }
        //设置连续时间测量中的时间间隔
        private void ContinuousTime(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                if (!(RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected is ContinuousTimeProfile)) return;
                //这里使用默认单位！
                try
                {
                    ((ContinuousTimeProfile)RuntimeGlobals.LMFTracker.Measurement.Profiles.Selected)
                        .TimeSeparation.Value = double.Parse(ContTimetxt.Text);
                }
                catch
                {
                    //MessageBox.Show("Please Input a Number!");
                }
            }
        }

        //设置Triggers
        private void ProbeButtonTrigger_Checked(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _measurenModel.SetTriggers(typeof(ProbeButtonTrigger), MeasOnProbeButtonDn.Checked);
            }
        }

        public bool IsTriggersSelected(Trigger paramTrigger)
        {
            if (paramTrigger is StableProbingTrigger)
            {
                if (MeasStablecb.Checked) return true;
                else return false;
            }
            else
            {
                if (paramTrigger is ProbeButtonTrigger)
                {
                    if (MeasOnProbeButtonDn.Checked) return true;
                    else return false;
                }
                else
                    return false;
            }
        }

        private void StableTrigger_Checked(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _measurenModel.SetTriggers(typeof(StableProbingTrigger), MeasStablecb.Checked);
            }
        }

        //开始一个新测量
        private void Startmeasure(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected() && !AsyncHelper.HasRunningTasks())
            {
                AsyncHelper.ExecuteAsync(() =>
                {
                    _measurenModel.StartMeasure();
                });
                WriteToExcelbt.Enabled = false;
            }
        }
        //停止测量
        public void StopMeasure_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected() && !AsyncHelper.HasRunningTasks())
            {
                AsyncHelper.ExecuteAsync(() =>
                {
                    _measurenModel.StopMeasure();
                });
                WriteToExcelbt.Enabled = true;
            }
        }
        //记录测量点
        public void RecordMeasurements(int count,string x, string y, string z, string q0 = null, string q1 = null, string q2 = null,string q3=null)
        {
            Invoke(new Action(() =>
            {
                SetMeasureColor("Measured");
                string inform = $"The {count} measured point:";
                string t1 = "X: ";
                string t2 = "Y: ";
                string t3 = "Z: ";

                string it1 = t1 + x + RuntimeGlobals.LengthUnit;
                string it2 = t2 + y + RuntimeGlobals.LengthUnit;
                string it3 = t3 + z + RuntimeGlobals.LengthUnit;

                MeasurementResultslst.Items.Clear();

                MeasurementResultslst.Items.Add(inform);
                MeasurementResultslst.Items.Add(it1);
                MeasurementResultslst.Items.Add(it2);
                MeasurementResultslst.Items.Add(it3);

                if (q0 == null || q1 == null || q2 == null || q3 == null) return;

                string t4 = "R0: ";
                string t5 = "R1: ";
                string t6 = "R2: ";
                string t7 = "R3: ";

                string it4 = t4 + q0 + RuntimeGlobals.AngleUnit;
                string it5 = t5 + q1 + RuntimeGlobals.AngleUnit;
                string it6 = t6 + q2 + RuntimeGlobals.AngleUnit;
                string it7 = t7 + q3 + RuntimeGlobals.AngleUnit;

                MeasurementResultslst.Items.Add(it4);
                MeasurementResultslst.Items.Add(it5);
                MeasurementResultslst.Items.Add(it6);
                MeasurementResultslst.Items.Add(it7);

            }));
        }
        //设置测量中与测量完成时的颜色变化
        public void SetMeasureColor(string color)
        {
            switch (color)
            {
                case "Measuring":
                    MeasurementResultslst.BackColor = Color.Red;
                    break;
                case "Measured":
                    MeasurementResultslst.BackColor = Color.Yellow;
                    break;
                case "StopMeasure":
                    MeasurementResultslst.BackColor = Color.White;
                    break;
                default: break;
            }
        }
        /*
        //记录测量点 原函数
        public void RecordMeasurements(string x, string y, string z, string q0 = null, string q1 = null, string q2 = null, string q3 = null)
        {
            Invoke(new Action(() =>
            {
                string t1 = "X: ";
                string t2 = "Y: ";
                string t3 = "Z: ";

                string it1 = t1 + x + RuntimeGlobals.LengthUnit;
                string it2 = t2 + y + RuntimeGlobals.LengthUnit;
                string it3 = t3 + z + RuntimeGlobals.LengthUnit;

                MeasurementResultslst.Items.Add(it1);
                MeasurementResultslst.Items.Add(it2);
                MeasurementResultslst.Items.Add(it3);

                if (q0 == null || q1 == null || q2 == null || q3 == null) return;

                string t4 = "R0: ";
                string t5 = "R1: ";
                string t6 = "R2: ";
                string t7 = "R3: ";

                string it4 = t4 + q0 + RuntimeGlobals.AngleUnit;
                string it5 = t5 + q1 + RuntimeGlobals.AngleUnit;
                string it6 = t6 + q2 + RuntimeGlobals.AngleUnit;
                string it7 = t7 + q3 + RuntimeGlobals.AngleUnit;

                MeasurementResultslst.Items.Add(it4);
                MeasurementResultslst.Items.Add(it5);
                MeasurementResultslst.Items.Add(it6);
                MeasurementResultslst.Items.Add(it7);

            }));
        }*/

        private void MeasureStatSync(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected()&&!AsyncHelper.HasRunningTasks())
            {
                AsyncHelper.ExecuteAsync(() =>
                {
                    _measurenModel.MeasureStationary();
                });
                WriteToExcelbt.Enabled = false;
            }
        }

        public bool IsStationaryMeasurement(ref Measurement measResult)
        {
            Measurement meas1 = _measurenModel.MeasureStationary();
            if (meas1 == null)
                return false;
            else
            {
                measResult = meas1;
                return true;
            }
        }

        //将测量结果写入Excel
        public void WriteToExcel_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected() /*&& !AsyncHelper.HasRunningTasks()*/)
            {
                //此异步执行命令会引发线程错误
                //AsyncHelper.ExecuteAsync(() =>
                //{
                    _measurenModel.WriteToExcel();
                //});
            }
        }
        //设置长度单位
        private void MM_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetLengthUnits("mm");
                _connectPageModel.setUnits();
            }
        }

        private void M_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetLengthUnits("m");
                _connectPageModel.setUnits();
            }
        }

        private void Inch_Click(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetLengthUnits("inch");
                _connectPageModel.setUnits();
            }
        }
        //设置角度单位
        private void RadianSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetAngleUnit("rad");
                _connectPageModel.setUnits();
            }
            
        }

        private void DegreeSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetAngleUnit("deg");
                _connectPageModel.setUnits();
            }
        }

        private void GonSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetAngleUnit("gon");
                _connectPageModel.setUnits();
            }
        }
        //设置坐标系类型
        private void CartesianSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.SetCoordinate("cartesian");
            }
        }

        private void SphericalSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _menuModel.SetCoordinate("spherical");
            
        }

        private void CylindricalSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _menuModel.SetCoordinate("cylindrical");
            
        }
        //设置旋转类型
        private void RtQuartSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _menuModel.SetRotationType("quar");
        }

        private void RtRPYSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _menuModel.SetRotationType("rpy");
        }

        private void RtAnglesSet(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
                _menuModel.SetRotationType("angle");
        }

        private void IpAddressChanged(object sender, EventArgs e)
        {
            _ipAddress = IPAddress.Text;
        }

        private void OVCFunction(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.ShowOVC();
            }
        }

        private void AlignmentInitialize(object sender, EventArgs e)
        {
            if (_connectPageModel.IsTrackerConnected())
            {
                _menuModel.ShowAlignment();
            }
        }

        //Tracker异常输出
        public void ErrorOutput(LmfError error)
        {
            ErrorDescriptiontb.Text = string.Format("Title:{0}{1}Description:{2}{3}Solution: {4}", error.Title,
                Environment.NewLine, error.Description, Environment.NewLine, error.Solution);
        }

        private void GetDescription_Click(object sender, EventArgs e)
        {
            int errorNumber;
            if (!string.IsNullOrEmpty(ErrorNumbertb.Text))
            {
                errorNumber = int.Parse(ErrorNumbertb.Text);
                if (errorNumber != 0)
                {
                    _trackerErrorModel.GetErrorDescription(errorNumber);
                }
            }
        }

        //************************显示几个About窗口***************************
        private void OvcAbout_Click(object sender, EventArgs e)
        {
            _menuModel.ShowOVCAbout();
        }

        private void ShowAlignmentAboutDialog_Click(object sender, EventArgs e)
        {
            _menuModel.ShowAlignmentAbout();
        }

        private void CloseAll_Click(object sender, EventArgs e)
        {
            ClearAllModes();
            _connectPageModel.Disconnect();
            this.Close();
        }

        private void ShowAbout_Click(object sender, EventArgs e)
        {
            _menuModel.ShowAbout();
        }

        private void OpenMeasurementfile_Click(object sender, EventArgs e)
        {
            OpenFileDialog measfile=new OpenFileDialog();
            measfile.InitialDirectory = Application.StartupPath;
            measfile.Title = "请选择需要打开的测量结果文件：";
            measfile.Filter = "表格(*.xlsx)|*.xlsx";
            string path = "";
            if (measfile.ShowDialog() == DialogResult.OK)
            {
                path = measfile.FileName;
                System.Diagnostics.Process.Start(path);
            }
        }
    }
}
