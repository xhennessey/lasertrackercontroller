﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 这里涉及到线程，不懂
namespace LaserTrackerController
{
    class AsyncHelper
    {
        private static List<Task> TaskList=new List<Task>();

        public static async  void ExecuteAsync(Action async, Action<Exception> finish)
        {
            var t = Task.Run(async);
            TaskList.Add(t);
            try
            {
                await t;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception in AsyncHelper", MessageBoxButtons.OK, MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            finally
            {
                if (finish != null)
                    finish.Invoke(t.Exception);
                TaskList.Remove(t);
            }
        }

        public static void ExecuteAsync(Action method)
        {
            ExecuteAsync(method,null);
        }

        public static bool HasRunningTasks()
        {
            return TaskList.Any(t => t.Status == TaskStatus.Running);
        }
    }
}
