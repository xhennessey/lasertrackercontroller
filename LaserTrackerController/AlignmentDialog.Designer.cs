﻿namespace LaserTrackerController
{
    partial class AlignmentDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.AlignmentOutput = new System.Windows.Forms.GroupBox();
            this.Closebt = new System.Windows.Forms.Button();
            this.ClearTransbt = new System.Windows.Forms.Button();
            this.SetTransformationbt = new System.Windows.Forms.Button();
            this.CalculateTransbt = new System.Windows.Forms.Button();
            this.ClearOrienbt = new System.Windows.Forms.Button();
            this.SetOrientationbt = new System.Windows.Forms.Button();
            this.CalculateOrienbt = new System.Windows.Forms.Button();
            this.AlignmentOutputlst = new System.Windows.Forms.ListBox();
            this.MeasurementParams = new System.Windows.Forms.GroupBox();
            this.MeasZaccbx = new System.Windows.Forms.TextBox();
            this.MeasZbx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.MeasYaccbx = new System.Windows.Forms.TextBox();
            this.MeasYbx = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.MeasXaccbx = new System.Windows.Forms.TextBox();
            this.MeasXbx = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.AddConstraintgrp = new System.Windows.Forms.GroupBox();
            this.AddConstraintbt = new System.Windows.Forms.Button();
            this.ConsScalecbx = new System.Windows.Forms.ComboBox();
            this.ConsScalecb = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ConsRzcbx = new System.Windows.Forms.ComboBox();
            this.ConsRzcb = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ConsRycbx = new System.Windows.Forms.ComboBox();
            this.ConsRycb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ConsRxcbx = new System.Windows.Forms.ComboBox();
            this.ConsRxcb = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ConsTzcbx = new System.Windows.Forms.ComboBox();
            this.ConsTztb = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ConsTycbx = new System.Windows.Forms.ComboBox();
            this.ConsTytb = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ConsTxcbx = new System.Windows.Forms.ComboBox();
            this.ConsTxtb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AddPointFromMeasbt = new System.Windows.Forms.Button();
            this.AddPointbt = new System.Windows.Forms.Button();
            this.Pointstb = new System.Windows.Forms.TextBox();
            this.ClearPointList = new System.Windows.Forms.Button();
            this.NominalParams = new System.Windows.Forms.GroupBox();
            this.NominalZaccbx = new System.Windows.Forms.TextBox();
            this.NominalZbx = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NominalYaccbx = new System.Windows.Forms.TextBox();
            this.NominalYbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NominalXaccbx = new System.Windows.Forms.TextBox();
            this.NominalXbx = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.AlignmentOutput.SuspendLayout();
            this.MeasurementParams.SuspendLayout();
            this.AddConstraintgrp.SuspendLayout();
            this.NominalParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.AlignmentOutput);
            this.panel1.Controls.Add(this.MeasurementParams);
            this.panel1.Controls.Add(this.AddConstraintgrp);
            this.panel1.Controls.Add(this.AddPointFromMeasbt);
            this.panel1.Controls.Add(this.AddPointbt);
            this.panel1.Controls.Add(this.Pointstb);
            this.panel1.Controls.Add(this.ClearPointList);
            this.panel1.Controls.Add(this.NominalParams);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(609, 422);
            this.panel1.TabIndex = 0;
            // 
            // AlignmentOutput
            // 
            this.AlignmentOutput.Controls.Add(this.Closebt);
            this.AlignmentOutput.Controls.Add(this.ClearTransbt);
            this.AlignmentOutput.Controls.Add(this.SetTransformationbt);
            this.AlignmentOutput.Controls.Add(this.CalculateTransbt);
            this.AlignmentOutput.Controls.Add(this.ClearOrienbt);
            this.AlignmentOutput.Controls.Add(this.SetOrientationbt);
            this.AlignmentOutput.Controls.Add(this.CalculateOrienbt);
            this.AlignmentOutput.Controls.Add(this.AlignmentOutputlst);
            this.AlignmentOutput.Location = new System.Drawing.Point(399, 5);
            this.AlignmentOutput.Name = "AlignmentOutput";
            this.AlignmentOutput.Size = new System.Drawing.Size(207, 414);
            this.AlignmentOutput.TabIndex = 8;
            this.AlignmentOutput.TabStop = false;
            this.AlignmentOutput.Text = "Alignment Output";
            // 
            // Closebt
            // 
            this.Closebt.Location = new System.Drawing.Point(126, 385);
            this.Closebt.Name = "Closebt";
            this.Closebt.Size = new System.Drawing.Size(75, 23);
            this.Closebt.TabIndex = 7;
            this.Closebt.Text = "Close";
            this.Closebt.UseVisualStyleBackColor = true;
            this.Closebt.Click += new System.EventHandler(this.Close_Alignment);
            // 
            // ClearTransbt
            // 
            this.ClearTransbt.Enabled = false;
            this.ClearTransbt.Location = new System.Drawing.Point(7, 145);
            this.ClearTransbt.Name = "ClearTransbt";
            this.ClearTransbt.Size = new System.Drawing.Size(194, 23);
            this.ClearTransbt.TabIndex = 6;
            this.ClearTransbt.Text = "Clear Transformation";
            this.ClearTransbt.UseVisualStyleBackColor = true;
            this.ClearTransbt.Click += new System.EventHandler(this.ClearTransformation_Click);
            // 
            // SetTransformationbt
            // 
            this.SetTransformationbt.Enabled = false;
            this.SetTransformationbt.Location = new System.Drawing.Point(7, 120);
            this.SetTransformationbt.Name = "SetTransformationbt";
            this.SetTransformationbt.Size = new System.Drawing.Size(194, 23);
            this.SetTransformationbt.TabIndex = 5;
            this.SetTransformationbt.Text = "Set As Transformation";
            this.SetTransformationbt.UseVisualStyleBackColor = true;
            this.SetTransformationbt.Click += new System.EventHandler(this.SetAsTransformation_Click);
            // 
            // CalculateTransbt
            // 
            this.CalculateTransbt.Location = new System.Drawing.Point(7, 95);
            this.CalculateTransbt.Name = "CalculateTransbt";
            this.CalculateTransbt.Size = new System.Drawing.Size(194, 23);
            this.CalculateTransbt.TabIndex = 4;
            this.CalculateTransbt.Text = "Calculate Transformation";
            this.CalculateTransbt.UseVisualStyleBackColor = true;
            this.CalculateTransbt.Click += new System.EventHandler(this.CalculateTransbt_Click);
            // 
            // ClearOrienbt
            // 
            this.ClearOrienbt.Enabled = false;
            this.ClearOrienbt.Location = new System.Drawing.Point(7, 71);
            this.ClearOrienbt.Name = "ClearOrienbt";
            this.ClearOrienbt.Size = new System.Drawing.Size(194, 23);
            this.ClearOrienbt.TabIndex = 3;
            this.ClearOrienbt.Text = "Clear Orientation";
            this.ClearOrienbt.UseVisualStyleBackColor = true;
            this.ClearOrienbt.Click += new System.EventHandler(this.ClearOrientation_Click);
            // 
            // SetOrientationbt
            // 
            this.SetOrientationbt.Enabled = false;
            this.SetOrientationbt.Location = new System.Drawing.Point(7, 45);
            this.SetOrientationbt.Name = "SetOrientationbt";
            this.SetOrientationbt.Size = new System.Drawing.Size(194, 23);
            this.SetOrientationbt.TabIndex = 2;
            this.SetOrientationbt.Text = "Set As Orientation";
            this.SetOrientationbt.UseVisualStyleBackColor = true;
            this.SetOrientationbt.Click += new System.EventHandler(this.SetAsOrientation_Click);
            // 
            // CalculateOrienbt
            // 
            this.CalculateOrienbt.Location = new System.Drawing.Point(7, 20);
            this.CalculateOrienbt.Name = "CalculateOrienbt";
            this.CalculateOrienbt.Size = new System.Drawing.Size(194, 23);
            this.CalculateOrienbt.TabIndex = 1;
            this.CalculateOrienbt.Text = "Calculate Orientation";
            this.CalculateOrienbt.UseVisualStyleBackColor = true;
            this.CalculateOrienbt.Click += new System.EventHandler(this.CalculateOrienbt_Click);
            // 
            // AlignmentOutputlst
            // 
            this.AlignmentOutputlst.FormattingEnabled = true;
            this.AlignmentOutputlst.ItemHeight = 12;
            this.AlignmentOutputlst.Location = new System.Drawing.Point(7, 183);
            this.AlignmentOutputlst.Name = "AlignmentOutputlst";
            this.AlignmentOutputlst.Size = new System.Drawing.Size(194, 196);
            this.AlignmentOutputlst.TabIndex = 0;
            // 
            // MeasurementParams
            // 
            this.MeasurementParams.Controls.Add(this.MeasZaccbx);
            this.MeasurementParams.Controls.Add(this.MeasZbx);
            this.MeasurementParams.Controls.Add(this.label8);
            this.MeasurementParams.Controls.Add(this.label9);
            this.MeasurementParams.Controls.Add(this.MeasYaccbx);
            this.MeasurementParams.Controls.Add(this.MeasYbx);
            this.MeasurementParams.Controls.Add(this.label10);
            this.MeasurementParams.Controls.Add(this.label11);
            this.MeasurementParams.Controls.Add(this.MeasXaccbx);
            this.MeasurementParams.Controls.Add(this.MeasXbx);
            this.MeasurementParams.Controls.Add(this.label12);
            this.MeasurementParams.Controls.Add(this.label13);
            this.MeasurementParams.Location = new System.Drawing.Point(7, 81);
            this.MeasurementParams.Name = "MeasurementParams";
            this.MeasurementParams.Size = new System.Drawing.Size(386, 74);
            this.MeasurementParams.TabIndex = 7;
            this.MeasurementParams.TabStop = false;
            this.MeasurementParams.Text = "Measurement";
            // 
            // MeasZaccbx
            // 
            this.MeasZaccbx.Location = new System.Drawing.Point(312, 45);
            this.MeasZaccbx.Name = "MeasZaccbx";
            this.MeasZaccbx.Size = new System.Drawing.Size(65, 21);
            this.MeasZaccbx.TabIndex = 17;
            this.MeasZaccbx.Text = "0.00001";
            // 
            // MeasZbx
            // 
            this.MeasZbx.Location = new System.Drawing.Point(312, 20);
            this.MeasZbx.Name = "MeasZbx";
            this.MeasZbx.Size = new System.Drawing.Size(65, 21);
            this.MeasZbx.TabIndex = 16;
            this.MeasZbx.Text = "0.0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(262, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 15;
            this.label8.Text = "Z[m]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(262, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "ZAcc[m]";
            // 
            // MeasYaccbx
            // 
            this.MeasYaccbx.Location = new System.Drawing.Point(183, 45);
            this.MeasYaccbx.Name = "MeasYaccbx";
            this.MeasYaccbx.Size = new System.Drawing.Size(65, 21);
            this.MeasYaccbx.TabIndex = 13;
            this.MeasYaccbx.Text = "0.00001";
            // 
            // MeasYbx
            // 
            this.MeasYbx.Location = new System.Drawing.Point(183, 20);
            this.MeasYbx.Name = "MeasYbx";
            this.MeasYbx.Size = new System.Drawing.Size(65, 21);
            this.MeasYbx.TabIndex = 12;
            this.MeasYbx.Text = "0.0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(133, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 11;
            this.label10.Text = "Y[m]";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(133, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 10;
            this.label11.Text = "YAcc[m]";
            // 
            // MeasXaccbx
            // 
            this.MeasXaccbx.Location = new System.Drawing.Point(57, 45);
            this.MeasXaccbx.Name = "MeasXaccbx";
            this.MeasXaccbx.Size = new System.Drawing.Size(65, 21);
            this.MeasXaccbx.TabIndex = 9;
            this.MeasXaccbx.Text = "0.00001";
            // 
            // MeasXbx
            // 
            this.MeasXbx.Location = new System.Drawing.Point(57, 20);
            this.MeasXbx.Name = "MeasXbx";
            this.MeasXbx.Size = new System.Drawing.Size(65, 21);
            this.MeasXbx.TabIndex = 8;
            this.MeasXbx.Text = "0.0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 7;
            this.label12.Text = "X[m]";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "XAcc[m]";
            // 
            // AddConstraintgrp
            // 
            this.AddConstraintgrp.Controls.Add(this.AddConstraintbt);
            this.AddConstraintgrp.Controls.Add(this.ConsScalecbx);
            this.AddConstraintgrp.Controls.Add(this.ConsScalecb);
            this.AddConstraintgrp.Controls.Add(this.label19);
            this.AddConstraintgrp.Controls.Add(this.ConsRzcbx);
            this.AddConstraintgrp.Controls.Add(this.ConsRzcb);
            this.AddConstraintgrp.Controls.Add(this.label18);
            this.AddConstraintgrp.Controls.Add(this.ConsRycbx);
            this.AddConstraintgrp.Controls.Add(this.ConsRycb);
            this.AddConstraintgrp.Controls.Add(this.label17);
            this.AddConstraintgrp.Controls.Add(this.ConsRxcbx);
            this.AddConstraintgrp.Controls.Add(this.ConsRxcb);
            this.AddConstraintgrp.Controls.Add(this.label16);
            this.AddConstraintgrp.Controls.Add(this.ConsTzcbx);
            this.AddConstraintgrp.Controls.Add(this.ConsTztb);
            this.AddConstraintgrp.Controls.Add(this.label15);
            this.AddConstraintgrp.Controls.Add(this.ConsTycbx);
            this.AddConstraintgrp.Controls.Add(this.ConsTytb);
            this.AddConstraintgrp.Controls.Add(this.label14);
            this.AddConstraintgrp.Controls.Add(this.ConsTxcbx);
            this.AddConstraintgrp.Controls.Add(this.ConsTxtb);
            this.AddConstraintgrp.Controls.Add(this.label1);
            this.AddConstraintgrp.Location = new System.Drawing.Point(7, 188);
            this.AddConstraintgrp.Name = "AddConstraintgrp";
            this.AddConstraintgrp.Size = new System.Drawing.Size(386, 231);
            this.AddConstraintgrp.TabIndex = 6;
            this.AddConstraintgrp.TabStop = false;
            this.AddConstraintgrp.Text = "Add Constraint";
            // 
            // AddConstraintbt
            // 
            this.AddConstraintbt.Location = new System.Drawing.Point(263, 202);
            this.AddConstraintbt.Name = "AddConstraintbt";
            this.AddConstraintbt.Size = new System.Drawing.Size(114, 23);
            this.AddConstraintbt.TabIndex = 21;
            this.AddConstraintbt.Text = "Add Constraint";
            this.AddConstraintbt.UseVisualStyleBackColor = true;
            this.AddConstraintbt.Click += new System.EventHandler(this.AddConstraint_Click);
            // 
            // ConsScalecbx
            // 
            this.ConsScalecbx.FormattingEnabled = true;
            this.ConsScalecbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsScalecbx.Location = new System.Drawing.Point(233, 177);
            this.ConsScalecbx.Name = "ConsScalecbx";
            this.ConsScalecbx.Size = new System.Drawing.Size(145, 20);
            this.ConsScalecbx.TabIndex = 20;
            // 
            // ConsScalecb
            // 
            this.ConsScalecb.Location = new System.Drawing.Point(122, 176);
            this.ConsScalecb.Name = "ConsScalecb";
            this.ConsScalecb.Size = new System.Drawing.Size(105, 21);
            this.ConsScalecb.TabIndex = 19;
            this.ConsScalecb.Text = "1.0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 179);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 12);
            this.label19.TabIndex = 18;
            this.label19.Text = "Scale";
            // 
            // ConsRzcbx
            // 
            this.ConsRzcbx.FormattingEnabled = true;
            this.ConsRzcbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsRzcbx.Location = new System.Drawing.Point(233, 152);
            this.ConsRzcbx.Name = "ConsRzcbx";
            this.ConsRzcbx.Size = new System.Drawing.Size(145, 20);
            this.ConsRzcbx.TabIndex = 17;
            // 
            // ConsRzcb
            // 
            this.ConsRzcb.Location = new System.Drawing.Point(122, 151);
            this.ConsRzcb.Name = "ConsRzcb";
            this.ConsRzcb.Size = new System.Drawing.Size(105, 21);
            this.ConsRzcb.TabIndex = 16;
            this.ConsRzcb.Text = "0.0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 154);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 12);
            this.label18.TabIndex = 15;
            this.label18.Text = "Rotation Z[rad]";
            // 
            // ConsRycbx
            // 
            this.ConsRycbx.FormattingEnabled = true;
            this.ConsRycbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsRycbx.Location = new System.Drawing.Point(233, 126);
            this.ConsRycbx.Name = "ConsRycbx";
            this.ConsRycbx.Size = new System.Drawing.Size(145, 20);
            this.ConsRycbx.TabIndex = 14;
            // 
            // ConsRycb
            // 
            this.ConsRycb.Location = new System.Drawing.Point(122, 125);
            this.ConsRycb.Name = "ConsRycb";
            this.ConsRycb.Size = new System.Drawing.Size(105, 21);
            this.ConsRycb.TabIndex = 13;
            this.ConsRycb.Text = "0.0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 12);
            this.label17.TabIndex = 12;
            this.label17.Text = "Rotation Y[rad]";
            // 
            // ConsRxcbx
            // 
            this.ConsRxcbx.FormattingEnabled = true;
            this.ConsRxcbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsRxcbx.Location = new System.Drawing.Point(233, 99);
            this.ConsRxcbx.Name = "ConsRxcbx";
            this.ConsRxcbx.Size = new System.Drawing.Size(145, 20);
            this.ConsRxcbx.TabIndex = 11;
            // 
            // ConsRxcb
            // 
            this.ConsRxcb.Location = new System.Drawing.Point(122, 98);
            this.ConsRxcb.Name = "ConsRxcb";
            this.ConsRxcb.Size = new System.Drawing.Size(105, 21);
            this.ConsRxcb.TabIndex = 10;
            this.ConsRxcb.Text = "0.0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 101);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(95, 12);
            this.label16.TabIndex = 9;
            this.label16.Text = "Rotation X[rad]";
            // 
            // ConsTzcbx
            // 
            this.ConsTzcbx.FormattingEnabled = true;
            this.ConsTzcbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsTzcbx.Location = new System.Drawing.Point(233, 72);
            this.ConsTzcbx.Name = "ConsTzcbx";
            this.ConsTzcbx.Size = new System.Drawing.Size(145, 20);
            this.ConsTzcbx.TabIndex = 8;
            // 
            // ConsTztb
            // 
            this.ConsTztb.Location = new System.Drawing.Point(122, 71);
            this.ConsTztb.Name = "ConsTztb";
            this.ConsTztb.Size = new System.Drawing.Size(105, 21);
            this.ConsTztb.TabIndex = 7;
            this.ConsTztb.Text = "0.0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 74);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 12);
            this.label15.TabIndex = 6;
            this.label15.Text = "Translation Z[m]";
            // 
            // ConsTycbx
            // 
            this.ConsTycbx.FormattingEnabled = true;
            this.ConsTycbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsTycbx.Location = new System.Drawing.Point(233, 46);
            this.ConsTycbx.Name = "ConsTycbx";
            this.ConsTycbx.Size = new System.Drawing.Size(145, 20);
            this.ConsTycbx.TabIndex = 5;
            // 
            // ConsTytb
            // 
            this.ConsTytb.Location = new System.Drawing.Point(122, 45);
            this.ConsTytb.Name = "ConsTytb";
            this.ConsTytb.Size = new System.Drawing.Size(105, 21);
            this.ConsTytb.TabIndex = 4;
            this.ConsTytb.Text = "0.0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 12);
            this.label14.TabIndex = 3;
            this.label14.Text = "Translation Y[m]";
            // 
            // ConsTxcbx
            // 
            this.ConsTxcbx.FormattingEnabled = true;
            this.ConsTxcbx.Items.AddRange(new object[] {
            "Unknown Accuracy",
            "Fixed Accuracy",
            "Approximate Accuracy"});
            this.ConsTxcbx.Location = new System.Drawing.Point(233, 20);
            this.ConsTxcbx.Name = "ConsTxcbx";
            this.ConsTxcbx.Size = new System.Drawing.Size(145, 20);
            this.ConsTxcbx.TabIndex = 2;
            // 
            // ConsTxtb
            // 
            this.ConsTxtb.Location = new System.Drawing.Point(122, 19);
            this.ConsTxtb.Name = "ConsTxtb";
            this.ConsTxtb.Size = new System.Drawing.Size(105, 21);
            this.ConsTxtb.TabIndex = 1;
            this.ConsTxtb.Text = "0.0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Translation X[m]";
            // 
            // AddPointFromMeasbt
            // 
            this.AddPointFromMeasbt.Location = new System.Drawing.Point(279, 161);
            this.AddPointFromMeasbt.Name = "AddPointFromMeasbt";
            this.AddPointFromMeasbt.Size = new System.Drawing.Size(114, 23);
            this.AddPointFromMeasbt.TabIndex = 5;
            this.AddPointFromMeasbt.Text = "AddFromMeas";
            this.AddPointFromMeasbt.UseVisualStyleBackColor = true;
            this.AddPointFromMeasbt.Click += new System.EventHandler(this.AddPointFromMeas_Click);
            // 
            // AddPointbt
            // 
            this.AddPointbt.Location = new System.Drawing.Point(190, 161);
            this.AddPointbt.Name = "AddPointbt";
            this.AddPointbt.Size = new System.Drawing.Size(83, 23);
            this.AddPointbt.TabIndex = 4;
            this.AddPointbt.Text = "AddPoint";
            this.AddPointbt.UseVisualStyleBackColor = true;
            this.AddPointbt.Click += new System.EventHandler(this.AddPointbt_Click);
            // 
            // Pointstb
            // 
            this.Pointstb.Enabled = false;
            this.Pointstb.Location = new System.Drawing.Point(103, 163);
            this.Pointstb.Name = "Pointstb";
            this.Pointstb.Size = new System.Drawing.Size(81, 21);
            this.Pointstb.TabIndex = 3;
            this.Pointstb.Text = "0";
            // 
            // ClearPointList
            // 
            this.ClearPointList.Location = new System.Drawing.Point(7, 161);
            this.ClearPointList.Name = "ClearPointList";
            this.ClearPointList.Size = new System.Drawing.Size(90, 23);
            this.ClearPointList.TabIndex = 2;
            this.ClearPointList.Text = "ClearPoints";
            this.ClearPointList.UseVisualStyleBackColor = true;
            this.ClearPointList.Click += new System.EventHandler(this.ClearPoint_CLick);
            // 
            // NominalParams
            // 
            this.NominalParams.Controls.Add(this.NominalZaccbx);
            this.NominalParams.Controls.Add(this.NominalZbx);
            this.NominalParams.Controls.Add(this.label6);
            this.NominalParams.Controls.Add(this.label7);
            this.NominalParams.Controls.Add(this.NominalYaccbx);
            this.NominalParams.Controls.Add(this.NominalYbx);
            this.NominalParams.Controls.Add(this.label4);
            this.NominalParams.Controls.Add(this.label5);
            this.NominalParams.Controls.Add(this.NominalXaccbx);
            this.NominalParams.Controls.Add(this.NominalXbx);
            this.NominalParams.Controls.Add(this.label2);
            this.NominalParams.Controls.Add(this.label3);
            this.NominalParams.Location = new System.Drawing.Point(7, 5);
            this.NominalParams.Name = "NominalParams";
            this.NominalParams.Size = new System.Drawing.Size(386, 75);
            this.NominalParams.TabIndex = 0;
            this.NominalParams.TabStop = false;
            this.NominalParams.Text = "Nominal";
            // 
            // NominalZaccbx
            // 
            this.NominalZaccbx.Location = new System.Drawing.Point(312, 44);
            this.NominalZaccbx.Name = "NominalZaccbx";
            this.NominalZaccbx.Size = new System.Drawing.Size(65, 21);
            this.NominalZaccbx.TabIndex = 17;
            this.NominalZaccbx.Text = "0.0";
            // 
            // NominalZbx
            // 
            this.NominalZbx.Location = new System.Drawing.Point(312, 20);
            this.NominalZbx.Name = "NominalZbx";
            this.NominalZbx.Size = new System.Drawing.Size(65, 21);
            this.NominalZbx.TabIndex = 16;
            this.NominalZbx.Text = "0.0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(262, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "Z[m]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(262, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "ZAcc[m]";
            // 
            // NominalYaccbx
            // 
            this.NominalYaccbx.Location = new System.Drawing.Point(183, 44);
            this.NominalYaccbx.Name = "NominalYaccbx";
            this.NominalYaccbx.Size = new System.Drawing.Size(65, 21);
            this.NominalYaccbx.TabIndex = 13;
            this.NominalYaccbx.Text = "0.0";
            // 
            // NominalYbx
            // 
            this.NominalYbx.Location = new System.Drawing.Point(183, 20);
            this.NominalYbx.Name = "NominalYbx";
            this.NominalYbx.Size = new System.Drawing.Size(65, 21);
            this.NominalYbx.TabIndex = 12;
            this.NominalYbx.Text = "0.0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(133, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "Y[m]";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "YAcc[m]";
            // 
            // NominalXaccbx
            // 
            this.NominalXaccbx.Location = new System.Drawing.Point(57, 44);
            this.NominalXaccbx.Name = "NominalXaccbx";
            this.NominalXaccbx.Size = new System.Drawing.Size(65, 21);
            this.NominalXaccbx.TabIndex = 9;
            this.NominalXaccbx.Text = "0.0";
            // 
            // NominalXbx
            // 
            this.NominalXbx.Location = new System.Drawing.Point(57, 20);
            this.NominalXbx.Name = "NominalXbx";
            this.NominalXbx.Size = new System.Drawing.Size(65, 21);
            this.NominalXbx.TabIndex = 8;
            this.NominalXbx.Text = "0.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "X[m]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "XAcc[m]";
            // 
            // AlignmentDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 447);
            this.Controls.Add(this.panel1);
            this.Name = "AlignmentDialog";
            this.Text = "AllgnmentDialog";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.AlignmentOutput.ResumeLayout(false);
            this.MeasurementParams.ResumeLayout(false);
            this.MeasurementParams.PerformLayout();
            this.AddConstraintgrp.ResumeLayout(false);
            this.AddConstraintgrp.PerformLayout();
            this.NominalParams.ResumeLayout(false);
            this.NominalParams.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox AddConstraintgrp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddPointFromMeasbt;
        private System.Windows.Forms.Button AddPointbt;
        private System.Windows.Forms.TextBox Pointstb;
        private System.Windows.Forms.Button ClearPointList;
        private System.Windows.Forms.GroupBox NominalParams;
        private System.Windows.Forms.TextBox NominalZaccbx;
        private System.Windows.Forms.TextBox NominalZbx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NominalYaccbx;
        private System.Windows.Forms.TextBox NominalYbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NominalXaccbx;
        private System.Windows.Forms.TextBox NominalXbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox MeasurementParams;
        private System.Windows.Forms.TextBox MeasZaccbx;
        private System.Windows.Forms.TextBox MeasZbx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox MeasYaccbx;
        private System.Windows.Forms.TextBox MeasYbx;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MeasXaccbx;
        private System.Windows.Forms.TextBox MeasXbx;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ConsTxcbx;
        private System.Windows.Forms.TextBox ConsTxtb;
        private System.Windows.Forms.ComboBox ConsRzcbx;
        private System.Windows.Forms.TextBox ConsRzcb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox ConsRycbx;
        private System.Windows.Forms.TextBox ConsRycb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox ConsRxcbx;
        private System.Windows.Forms.TextBox ConsRxcb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox ConsTzcbx;
        private System.Windows.Forms.TextBox ConsTztb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox ConsTycbx;
        private System.Windows.Forms.TextBox ConsTytb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox ConsScalecbx;
        private System.Windows.Forms.TextBox ConsScalecb;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button AddConstraintbt;
        private System.Windows.Forms.GroupBox AlignmentOutput;
        private System.Windows.Forms.ListBox AlignmentOutputlst;
        private System.Windows.Forms.Button ClearTransbt;
        private System.Windows.Forms.Button SetTransformationbt;
        private System.Windows.Forms.Button CalculateTransbt;
        private System.Windows.Forms.Button ClearOrienbt;
        private System.Windows.Forms.Button SetOrientationbt;
        private System.Windows.Forms.Button CalculateOrienbt;
        private System.Windows.Forms.Button Closebt;
    }
}