﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker.Enums;
using LMF.Tracker.OVC;

namespace LaserTrackerController
{
    public partial class OVCdialog : Form
    {
        public OVCdialog()
        {
            InitializeComponent();
        }

        public void SetOVCModes()
        {
        }

        public void ClearModes()
        {
        }

        //显示OVC窗口,将OVC显示在相应的pictureBox中
        private void ShowOVC(object sender, EventArgs e)
        {
            RuntimeGlobals.LMFTracker.OverviewCamera.Dialog.Show();
        }

        //开始进行预览定位StartOVC，对疑似靶球的目标点进行锁定
        private void StartOVC(object sender, EventArgs e)
        {
            RuntimeGlobals.LMFTracker.OverviewCamera.StartAsync();
            RuntimeGlobals.LMFTracker.OverviewCamera.ImageArrived += OVCImageArrived;
        }
        //停止进行预览定位StopOVC，停止搜索靶球目标点
        private void StopOVC(object sender, EventArgs e)
        {
            RuntimeGlobals.LMFTracker.OverviewCamera.Stop();
            RuntimeGlobals.LMFTracker.OverviewCamera.ImageArrived -= OVCImageArrived;
            OVCpictureBox.Image = null;
        }

        //关闭OVC图片
        private void CloseOVCDialog(object sender, EventArgs e)
        {
            RuntimeGlobals.LMFTracker.OverviewCamera.Dialog.Close();
            StopOVC(null,null);
        }
        private void OVCImageArrived(OverviewCamera sender, ref byte[] image, ATRCoordinateCollection atrCoordinates)
        {
            SetImage(ref image);
        }
        //将图片在框中表示
        private void SetImage(ref byte[] imageBytes)
        {
            var ic=new ImageConverter(); //图片转换器
            try
            {
                var image = (Image) ic.ConvertFrom(imageBytes);
                var newImage=new Bitmap(image);
                OVCpictureBox.Image = newImage;
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception in OVC: " + e.Message, "OVC Error");
            }

        }
        //设置StillImageMode写入下拉框中,【静止图像】
        public void SetStillImageModes(Array modes)
        {
            StillImageModecmb.Text = EStillImageMode.Medium.ToString();
            foreach (var mode in modes)
            {
                StillImageModecmb.Items.Add(mode);
            }
        }
        //显示静止图片
        private void ShowStillImage(object sender, EventArgs e)
        {
            EStillImageMode selectedMode = EStillImageMode.Medium;
            foreach (var mode in Enum.GetValues(typeof(EStillImageMode)))
            {
                if (mode.Equals(StillImageModecmb.SelectedItem))
                {
                    selectedMode = (EStillImageMode)mode;
                }
            }
            RuntimeGlobals.LMFTracker.OverviewCamera.ImageArrived -= OVCImageArrived;
            byte[] stillImage = RuntimeGlobals.LMFTracker.OverviewCamera.GetStillImage(selectedMode);
            SetImage(ref stillImage);
        }
        //关闭对话框
        private void CloseDialog(object sender, EventArgs e)
        {
            CloseOVCDialog(null, null);
            this.Close();
        }
    }
}
