﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker;
using LMF.Tracker.Targets;
using LMF.Tracker.Targets.Probes.ActiveProbes;
using LMF.Tracker.Measurements;
using LMF.Tracker.MeasurementResults;
using LMF.Tracker.Measurements.Profiles;
using LMF.Tracker.Enums;

namespace LaserTrackerController.Models
{
    class PositionModel
    {
        private List<LockOnToken> _lockOnTokenList;

        public List<LockOnToken> LockOnTokenList { get => _lockOnTokenList; set => _lockOnTokenList = value; }

        public PositionModel()
        {
            //属性lockOnTokenList必须进行初始化，在构造函数中进行初始化比较方便，避免在外部类中进行显式初始化
            LockOnTokenList = new List<LockOnToken>();
        }

        public void SetPositionModes()
        {
        }
        public void ClearPositionModes()
        {
        }

        //**********************************Move*******************************
        //控制tracker运动定位
        private void MoveUp(int speed)
        {
            RuntimeGlobals.LMFTracker.Move(0, -speed);
        }

        private void MoveLeft(int speed)
        {
            RuntimeGlobals.LMFTracker.Move(speed, 0);
        }

        private void MoveDown(int speed)
        {
            RuntimeGlobals.LMFTracker.Move(0,speed);
        }

        private void MoveRight(int speed)
        {
            RuntimeGlobals.LMFTracker.Move(-speed,0);
        }

        private void StopMove()
        {
            RuntimeGlobals.LMFTracker.StopMove();
        }

        public void Move(string speed, string direction)
        {
            int ispeed = int.Parse(speed);
            if (ispeed <= 0)
            {
                Console.WriteLine("Wrong in Move : MoveSpeed <= 0 !");
                return;
            }
            switch (direction)
            {
                case "up": MoveUp(ispeed); break;
                case "down": MoveDown(ispeed); break;
                case "left": MoveLeft(ispeed); break;
                case "right": MoveRight(ispeed); break;
                case "stop": StopMove(); break;
                default: break;
            }
        }

        //GoBirdBath (AT901)
        public void GoBirdBath()
        {
            if (RuntimeGlobals.LMFTracker is AT901Tracker tracker)
                tracker.GoBirdBath();
            else
            {
                MessageBox.Show("The tracker is not AT901!","WrongMessage",MessageBoxButtons.OK);
            }
        }

        //*****************************PositionTo*******************************
        public void PositionToSpecific(bool searchTarget, bool isrelative, double x, double y, double z,bool isAsync)
        {
            try
            {
                if (!isAsync)
                    RuntimeGlobals.LMFTracker.PositionTo(searchTarget, isrelative, x, y, z);
                else
                    RuntimeGlobals.LMFTracker.PositionToAsync(searchTarget, isrelative, x, y, z);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong in PositionTo:\n" + ex.Message);
                //throw;
            }
        }

        //***************************PositionToTargets***************************
        //1.获取LockOnToken信息，利用该类对象可以指定目标类型和Tracker的测量face，正面为1，反面为2
        public void GetLockOnToken()
        {
            LockOnTokenList.Clear();
            //LockOnToken rrr15 = RuntimeGlobals.LMFTracker.Targets.CreateLockOnToken("RRR 1.5in", null);
            //// lockOnTokenList.Add(rrr15);
            //LockOnToken rrr05 = RuntimeGlobals.LMFTracker.Targets.CreateLockOnToken("RRR 0.5in", null);
            //// lockOnTokenList.Add(rrr05);
            Tracker tracker = RuntimeGlobals.LMFTracker;
            if (tracker != null)
            {
                var targetCollect = tracker.Targets;
                foreach (Target target in targetCollect)
                {
                    if (target is ActiveProbe)
                    {
                        var probe = target as ActiveProbe;
                        //Select使用了Linq查询技术，lambda表达式定义了一个委托
                        //添加face信息
                        LockOnTokenList.AddRange(probe.ProbeFaces.Select(f=>f.GetLockOnToken()));
                    }
                    //添加目标名称信息
                    LockOnTokenList.Add(target.GetLockOnToken());
                }
            }
        }
        //***************************2.PositionToTarget**************************
        public Target PositionToTarget(LockOnToken lockOnToken, bool isRelative, double x, double y, double z)
        {
            return RuntimeGlobals.LMFTracker.PositionToTarget(lockOnToken, isRelative, x, y, z);
        }
        //*************************3.GoandMeasureStationary***********************
        public Measurement GoandMeasure(LockOnToken lockOnToken, bool isRelative, double x, double y, double z)
        {
            //选用第一个配置文件
            RuntimeGlobals.LMFTracker.Measurement.Profiles.OfType<StationaryMeasurementProfile>().First().Select();
            //第一个配置文件中的精度值设定为fast
            RuntimeGlobals.LMFTracker.Measurement.Profiles.OfType<StationaryMeasurementProfile>().First().Accuracy
                .Value = EAccuracy.Fast;
            return RuntimeGlobals.LMFTracker.Measurement.GoAndMeasureStationary(lockOnToken, isRelative, x, y, z);
        }
    }
}
