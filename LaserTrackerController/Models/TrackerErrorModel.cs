﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker;
using LMF.Tracker.ErrorHandling;

namespace LaserTrackerController.Models
{
    class TrackerErrorModel
    {
        private TrackerErrors _trackerErrors;

        public TrackerErrorModel()
        {
            _trackerErrors=new TrackerErrors();
        }

        public void SetTrackerErrorModel()
        {
            RuntimeGlobals.LMFTracker.ErrorArrived+=new Tracker.ErrorArrivedHandler(ErrorArrived);
            RuntimeGlobals.LMFTracker.InformationArrived+=new Tracker.InformationArrivedHandler(InformationArrived);
            RuntimeGlobals.LMFTracker.WarningArrived+=new Tracker.WarningArrivedHandler(WarningArrived);
        }

        public void ClearTrackerErrorModel()
        {
            if (RuntimeGlobals.LMFTracker == null) return;
            RuntimeGlobals.LMFTracker.ErrorArrived -= new Tracker.ErrorArrivedHandler(ErrorArrived);
            RuntimeGlobals.LMFTracker.InformationArrived -= new Tracker.InformationArrivedHandler(InformationArrived);
            RuntimeGlobals.LMFTracker.WarningArrived -= new Tracker.WarningArrivedHandler(WarningArrived);

        }

        private void ErrorArrived(Tracker sender,LmfError error)
        {
            MessageBox.Show(error.Description, error.Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void InformationArrived(Tracker sender, LmfInformation info)
        {
            MessageBox.Show(info.Description, info.Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void WarningArrived(Tracker sender, LmfWarning warning)
        {
            MessageBox.Show(warning.Description, warning.Title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        //使用更为直接的方式输出异常
        public void GetErrorDescription(int number)
        {
            var error = _trackerErrors.GetErrorDescription(number);
            RuntimeGlobals.MainForm.ErrorOutput(error);
        }
    }
}
