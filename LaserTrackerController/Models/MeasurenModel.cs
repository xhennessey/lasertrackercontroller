﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker;
using LMF.Tracker.Adapters.TPI.EmsconDBWrapper;
using LMF.Tracker.MeasurementResults;
using LMF.Tracker.MeasurementStatus;
using LMF.Tracker.Targets;
using LMF.Tracker.Targets.Probes.ActiveProbes;
using LMF.Tracker.Targets.Reflectors;
using LMF.Tracker.Enums;
using LMF.Tracker.ErrorHandling;
using LMF.Tracker.Measurements;
using LMF.Tracker.Measurements.Profiles;
using LMF.Tracker.Triggers;
using System.Diagnostics;


namespace LaserTrackerController.Models
{
    class MeasurenModel
    {
        private List<Target> _targets;
        public List<Target> Targets { get => _targets; set => _targets = value; }

        private Target _preselectedTarget;
        public Target PreselectedTarget { get => _preselectedTarget; set => _preselectedTarget = value; }

        private Target _selectedTarget;
        public Target SelectedTarget { get => _selectedTarget; set => _selectedTarget = value; }

        private List<MeasurementProfile> _measprofiles;
        public List<MeasurementProfile> Measprofiles { get => _measprofiles; set => _measprofiles = value; }

        private string filePath;
        private List<List<string>> resultslst;
        private string newFilePath;
        private int dataNumber;

        public MeasurenModel()
        {
            InitializeMeas();
        }

        private void InitializeMeas()
        {
            Targets = new List<Target>();
            PreselectedTarget = null;
            SelectedTarget = null;
            Measprofiles = new List<MeasurementProfile>();
            filePath = null;
            resultslst = new List<List<string>>();
            newFilePath = null;
            dataNumber = 1;
        }

        public void SetModelEvents()
        {
            UpdataTargets(new TargetCollection());
            GetPreselectedTarget(null, RuntimeGlobals.LMFTracker.Targets.PreSelected);
            GetSelectedTarget(null, RuntimeGlobals.LMFTracker.Targets.Selected);
            RuntimeGlobals.MainForm.ShowActiveTips();
            UpdatePreconditions(RuntimeGlobals.LMFTracker.Measurement.Status.Preconditions);
            SetProfiles();

            filePath = RuntimeGlobals.CreatExcel();

            RuntimeGlobals.LMFTracker.Targets.Changed += UpdataTargets;
            RuntimeGlobals.LMFTracker.Targets.PreSelectedChanged += GetPreselectedTarget;
            RuntimeGlobals.LMFTracker.Targets.SelectedChanged += GetSelectedTarget;
            RuntimeGlobals.LMFTracker.Targets.TargetPositionChanged += UpdateTargetPositon;
            RuntimeGlobals.LMFTracker.Measurement.Status.Preconditions.Changed += UpdatePreconditions;
            RuntimeGlobals.LMFTracker.Measurement.MeasurementArrived += MeasurementResultsRecord;
            RuntimeGlobals.LMFTracker.Triggers.TriggerHappened += DealWithTriggers;
        }
        //清楚所有Measure格式
        public void ClearMeasureModes()
        {
            RuntimeGlobals.MainForm.SetTargetList(null);
            RuntimeGlobals.MainForm.UpdatePreselectTarget(null);
            RuntimeGlobals.MainForm.UpdateSelectedTarget(null);
            RuntimeGlobals.MainForm.ShowActiveTips();
            RuntimeGlobals.MainForm.ShowPos(null, null, null);
            RuntimeGlobals.MainForm.ClearMeasureModes();
            InitializeMeas();

            if (RuntimeGlobals.LMFTracker == null) return;
            RuntimeGlobals.LMFTracker.Targets.Changed -= UpdataTargets;
            RuntimeGlobals.LMFTracker.Targets.PreSelectedChanged -= GetPreselectedTarget;
            RuntimeGlobals.LMFTracker.Targets.SelectedChanged -= GetSelectedTarget;
            RuntimeGlobals.LMFTracker.Targets.TargetPositionChanged -= UpdateTargetPositon;
            RuntimeGlobals.LMFTracker.Measurement.Status.Preconditions.Changed -= UpdatePreconditions;
            RuntimeGlobals.LMFTracker.Measurement.MeasurementArrived -= MeasurementResultsRecord;
            RuntimeGlobals.LMFTracker.Triggers.TriggerHappened -= DealWithTriggers;
            
        }

        private void UpdataTargets(TargetCollection sender)
        {
            Targets.Clear();
            var tracker = RuntimeGlobals.LMFTracker;
            foreach (Target detectTar in tracker.Targets)
            {
                Targets.Add(detectTar);
            }
            RuntimeGlobals.MainForm.SetTargetList(Targets);
        }

        private void GetPreselectedTarget(TargetCollection sender,Target paramTarget)
        {
            PreselectedTarget = paramTarget;
            RuntimeGlobals.MainForm.UpdatePreselectTarget(PreselectedTarget);
        }

        private void GetSelectedTarget(TargetCollection sender, Target paramTarget)
        {
            RuntimeGlobals.MainForm.ClearTips();
            SelectedTarget = paramTarget;
            RuntimeGlobals.MainForm.UpdateSelectedTarget(SelectedTarget);
            RuntimeGlobals.MainForm.ShowActiveTips();
        }

        private void UpdateTargetPositon(Tracker sender, SingleShotMeasurement3D paramPosition)
        {
            if (paramPosition is SingleShotMeasurement6D)
            {
                var paramPositionn = paramPosition as SingleShotMeasurement6D;
                if (paramPositionn != null)
                {
                    string x = paramPositionn.Position.Coordinate1.Value.ToString();
                    string y = paramPositionn.Position.Coordinate2.Value.ToString();
                    string z = paramPositionn.Position.Coordinate3.Value.ToString();
                    string q0 = paramPositionn.Rotation.Value0.ToString();
                    string q1 = paramPositionn.Rotation.Value1.ToString();
                    string q2 = paramPositionn.Rotation.Value2.ToString();
                    string q3 = paramPositionn.Rotation.Value3.ToString();

                    RuntimeGlobals.MainForm.ShowPos(x, y, z, q0, q1, q2, q3);
                }
                return;
            }
            RuntimeGlobals.MainForm.ShowPos(paramPosition.Position.Coordinate1.Value.ToString(),paramPosition.Position.Coordinate2.Value.ToString(),paramPosition.Position.Coordinate3.Value.ToString());
        }

        private void UpdatePreconditions(MeasurementPreconditionCollection sender)
        {
            string[] preconditions = sender.Select(p => p.Title).ToArray();
            RuntimeGlobals.MainForm.ShowPreconditions(preconditions);
        }
        //显示Profiles
        private void SetProfiles()
        {
            MeasurementProfileCollection profilecollec = RuntimeGlobals.LMFTracker.Measurement.Profiles;
            MeasurementProfile defaultProfile = profilecollec.Selected;
            
            if (profilecollec != null)
            {
                if(Measprofiles!=null)
                    Measprofiles.Clear();
                Measprofiles.AddRange(profilecollec);
                string[] profiles = profilecollec.Select(p => p.Name).ToArray();
                RuntimeGlobals.MainForm.ShowProfiles(defaultProfile, profiles);
            }
        }
        //设置triggers
        public void SetTriggers(Type triggerType, bool isEnabled)
        {
            if (triggerType == typeof(StableProbingTrigger))
                RuntimeGlobals.LMFTracker.Triggers.OfType<StableProbingTrigger>().FirstOrDefault().IsEnabled.Value =
                    isEnabled;
            if (triggerType == typeof(ProbeButtonTrigger))
                RuntimeGlobals.LMFTracker.Triggers.OfType<ProbeButtonTrigger>().FirstOrDefault().IsEnabled.Value =
                    isEnabled;
        }

        public void DealWithTriggers(Trigger paramTrigger, TriggerEventData data)
        {
            if(paramTrigger is StableProbingTrigger)
            {
                if (RuntimeGlobals.MainForm.IsTriggersSelected(paramTrigger))
                {
                    StartMeasure();
                }
            }
            else
            {
                if (paramTrigger is ProbeButtonTrigger)
                {
                    if (RuntimeGlobals.MainForm.IsTriggersSelected(paramTrigger))
                    {
                        if(data.Action==EButtonAction.Down) StartMeasure();
                    }
                }
            }
        }

        //测量
        public void StartMeasure()
        {
            if (RuntimeGlobals.LMFTracker.Measurement.Status.Value == EMeasurementStatus.ReadyToMeasure)
            {
                SetMeasuringColor();
                RuntimeGlobals.LMFTracker.Measurement.StartMeasurement();
            }
        }

        public void StopMeasure()
        {
            RuntimeGlobals.LMFTracker.Measurement.StopMeasurement();
            SetStopMeasureColor();
        }

        private void MeasurementResultsRecord(MeasurementSettings sender, MeasurementCollection measurements,
            LmfException paramException)
        {
            string x, y, z, r0 ="0", r1 = "0", r2 = "0", r3 = "0";
            var measurement = measurements[0];
            string meastime = measurement.TimeStamp.ToString();
            //string meastime = DateTime.Now.ToString();
            string face = ((int) RuntimeGlobals.LMFTracker.Face.Value + 1).ToString();
            string cstype = RuntimeGlobals.LMFTracker.Settings.CoordinateType.ToString();

            if (measurement is SingleShotMeasurement6D)
            {
                x = ((SingleShotMeasurement6D)measurement).Position.Coordinate1.Value.ToString();
                y = ((SingleShotMeasurement6D)measurement).Position.Coordinate2.Value.ToString();
                z = ((SingleShotMeasurement6D)measurement).Position.Coordinate3.Value.ToString();
                r0 = ((SingleShotMeasurement6D) measurement).Rotation.Value0.Value.ToString();
                r1 = ((SingleShotMeasurement6D) measurement).Rotation.Value1.Value.ToString();
                r2 = ((SingleShotMeasurement6D) measurement).Rotation.Value2.Value.ToString();
                r3 = ((SingleShotMeasurement6D) measurement).Rotation.Value3.Value.ToString();
                RuntimeGlobals.MainForm.RecordMeasurements(dataNumber,x, y, z, r0, r1, r2, r3);
                string[] results = { (dataNumber++).ToString(),x, y, z, r0, r1, r2, r3,meastime,face,cstype };
                List<string> lst=new List<string>();
                lst.AddRange(results);
                resultslst.Add(lst);
            }
            else if (measurement is SingleShotMeasurement3D)
            {
                x = ((SingleShotMeasurement3D) measurement).Position.Coordinate1.Value.ToString();
                y = ((SingleShotMeasurement3D) measurement).Position.Coordinate2.Value.ToString();
                z = ((SingleShotMeasurement3D) measurement).Position.Coordinate3.Value.ToString();
                RuntimeGlobals.MainForm.RecordMeasurements(dataNumber, x, y, z);
                string[] results = { (dataNumber++).ToString(), x, y, z, r0, r1, r2, r3, meastime, face, cstype };
                List<string> lst = new List<string>();
                lst.AddRange(results);
                resultslst.Add(lst);
            }
            else if (measurement is StationaryMeasurement6D)
            {
                x = ((StationaryMeasurement6D)measurement).Position.Coordinate1.Value.ToString();
                y = ((StationaryMeasurement6D)measurement).Position.Coordinate2.Value.ToString();
                z = ((StationaryMeasurement6D)measurement).Position.Coordinate3.Value.ToString();
                r0 = ((StationaryMeasurement6D)measurement).Rotation.Value0.Value.ToString();
                r1 = ((StationaryMeasurement6D)measurement).Rotation.Value1.Value.ToString();
                r2 = ((StationaryMeasurement6D)measurement).Rotation.Value2.Value.ToString();
                r3 = ((StationaryMeasurement6D)measurement).Rotation.Value3.Value.ToString();
                RuntimeGlobals.MainForm.RecordMeasurements(dataNumber, x, y, z, r0, r1, r2, r3);
                string[] results = { (dataNumber++).ToString(), x, y, z, r0, r1, r2, r3, meastime, face, cstype };
                List<string> lst = new List<string>();
                lst.AddRange(results);
                resultslst.Add(lst);
            }
            else if (measurement is StationaryMeasurement3D)
            {
                x = ((StationaryMeasurement3D)measurement).Position.Coordinate1.Value.ToString();
                y = ((StationaryMeasurement3D)measurement).Position.Coordinate2.Value.ToString();
                z = ((StationaryMeasurement3D)measurement).Position.Coordinate3.Value.ToString();
                RuntimeGlobals.MainForm.RecordMeasurements(dataNumber, x, y, z);
                string[] results = { (dataNumber++).ToString(), x, y, z, r0, r1, r2, r3, meastime, face, cstype };
                List<string> lst = new List<string>();
                lst.AddRange(results);
                resultslst.Add(lst);
            }
        }

        public Measurement MeasureStationary()
        {
            SetMeasuringColor();
            if (RuntimeGlobals.LMFTracker.Measurement.Status.Value == EMeasurementStatus.ReadyToMeasure)
            {
                Measurement meas = RuntimeGlobals.LMFTracker.Measurement.MeasureStationary();
                if (meas == null) return null;
                string x, y, z, r0 = "0", r1 = "0", r2 = "0", r3 = "0";
                string meastime = meas.TimeStamp.ToString();
                //string meastime = DateTime.Now.ToString();
                //MessageBox.Show(meastime);
                string face = ((int)RuntimeGlobals.LMFTracker.Face.Value + 1).ToString();
                string cstype = RuntimeGlobals.LMFTracker.Settings.CoordinateType.ToString();

                if (meas is StationaryMeasurement6D)
                {
                    x = ((StationaryMeasurement6D)meas).Position.Coordinate1.Value.ToString();
                    y = ((StationaryMeasurement6D)meas).Position.Coordinate2.Value.ToString();
                    z = ((StationaryMeasurement6D)meas).Position.Coordinate3.Value.ToString();
                    r0 = ((StationaryMeasurement6D)meas).Rotation.Value0.Value.ToString();
                    r1 = ((StationaryMeasurement6D)meas).Rotation.Value1.Value.ToString();
                    r2 = ((StationaryMeasurement6D)meas).Rotation.Value2.Value.ToString();
                    r3 = ((StationaryMeasurement6D)meas).Rotation.Value3.Value.ToString();
                    RuntimeGlobals.MainForm.RecordMeasurements(dataNumber,x, y, z, r0, r1, r2, r3);
                    string[] results = { (dataNumber++).ToString(), x, y, z, r0, r1, r2, r3, meastime, face, cstype };
                    List<string> lst = new List<string>();
                    lst.AddRange(results);
                    resultslst.Add(lst);
                }
                else if (meas is StationaryMeasurement3D)
                {
                    x = ((StationaryMeasurement3D)meas).Position.Coordinate1.Value.ToString();
                    y = ((StationaryMeasurement3D)meas).Position.Coordinate2.Value.ToString();
                    z = ((StationaryMeasurement3D)meas).Position.Coordinate3.Value.ToString();
                    RuntimeGlobals.MainForm.RecordMeasurements(dataNumber, x, y, z);
                    string[] results = { (dataNumber++).ToString(), x, y, z, r0, r1, r2, r3, meastime, face, cstype };
                    List<string> lst = new List<string>();
                    lst.AddRange(results);
                    resultslst.Add(lst);
                }
                return meas;
            }
            else
            {
                return null;
            }
        }
        //设置测量时颜色
        private void SetMeasuringColor()
        {
            RuntimeGlobals.MainForm.SetMeasureColor("Measuring");
        }
        //设置停止测量时的颜色
        private void SetStopMeasureColor()
        {
            RuntimeGlobals.MainForm.SetMeasureColor("StopMeasure");
        }

        //将结果写入Excel中
        public void WriteToExcel()
        {
            if (resultslst.Count == 0) 
            {
                MessageBox.Show("No MeasurementResults!");
                return;
            }
            RuntimeGlobals.WriteExcel(resultslst, filePath, ref newFilePath);
            //RuntimeGlobals.ChangeFileName(filePath, newFilePath);
            MessageBox.Show("WriteToExcelSuccessful!");
        }
    }
}
