﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMF.Tracker;
using LMF.Units;
using LMF.Tracker.Enums;

namespace LaserTrackerController.Models
{
    class MenuModel
    {
        //设置基本模式
        public void SetBasicModels()
        {
            SetLengthUnits("mm");
            SetAngleUnit("deg");
            SetCoordinate("cartesian");
            SetRotationType("angle");
        }

        public void ClearMenuModes()
        {
        }

        // 设置单位
        public void SetLengthUnits(string lengthunit)
        {
            switch (lengthunit)
            {
                case "m":
                    RuntimeGlobals.LMFTracker.Settings.Units.LengthUnit = ELengthUnit.Meter;
                    break;
                case "mm":
                    RuntimeGlobals.LMFTracker.Settings.Units.LengthUnit = ELengthUnit.Millimeter;
                    break;
                case "inch":
                    RuntimeGlobals.LMFTracker.Settings.Units.LengthUnit = ELengthUnit.Inch;
                    break;
                    default:break;
            }
        }

        public void SetAngleUnit(string angleunit)
        {
            switch (angleunit)
            {
                case "rad":
                    RuntimeGlobals.LMFTracker.Settings.Units.AngleUnit = EAngleUnit.Radian;
                    break;
                case "gon":
                    RuntimeGlobals.LMFTracker.Settings.Units.AngleUnit = EAngleUnit.Gon;
                    break;
                case "deg":
                    RuntimeGlobals.LMFTracker.Settings.Units.AngleUnit = EAngleUnit.Degree;
                    break;
                    default:break;
            }
        }

        // 设置坐标系
        public void SetCoordinate(string coordinate)
        {
            switch (coordinate)
            {
                case "cartesian":
                    RuntimeGlobals.LMFTracker.Settings.CoordinateType = ECoordinateType.Cartesian;
                    break;
                case "spherical":
                    RuntimeGlobals.LMFTracker.Settings.CoordinateType = ECoordinateType.Spherical;
                    break;
                case "cylindrical":
                    RuntimeGlobals.LMFTracker.Settings.CoordinateType = ECoordinateType.Cylindrical;
                    break;
                    default:break;
            }
        }
        //设置旋转角类型
        public void SetRotationType(string rotation)
        {
            switch (rotation)
            {
                case "quar":
                    RuntimeGlobals.LMFTracker.Settings.RotationType = ERotationType.Quaternion;
                    break;
                case "rpy":
                    RuntimeGlobals.LMFTracker.Settings.RotationType = ERotationType.RollPitchYaw;
                    break;
                case "angle":
                    RuntimeGlobals.LMFTracker.Settings.RotationType = ERotationType.RotationAngles;
                    break;
                    default:break;
            }
        }
        //设置OVC
        public void ShowOVC()
        {
            OVCdialog ovc = new OVCdialog();
            ovc.Show();
            var modes = Enum.GetValues(typeof(EStillImageMode));
            ovc.SetStillImageModes(modes);
        }

        public void ShowOVCAbout()
        {
            OVCAboutdialog ovcAbout=new OVCAboutdialog();
            ovcAbout.Show();
        }

        //设置Alignment
        public void ShowAlignment()
        {
            AlignmentDialog alignmentDia = new AlignmentDialog();
            alignmentDia.Show();
        }

        public void ShowAlignmentAbout()
        {
            AlignmentAboutDialog alignmentAbout=new AlignmentAboutDialog();
            alignmentAbout.Show();
        }
        
        //显示软件的About窗口
        public void ShowAbout()
        {
            AboutDialog about=new AboutDialog();
            about.Show();
        }
    }
}
