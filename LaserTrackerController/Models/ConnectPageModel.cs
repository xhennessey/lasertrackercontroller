﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMF.Tracker;
using LMF.Tracker.Enums;

namespace LaserTrackerController.Models
{
    class ConnectPageModel
    {
        public void InitializeTracker()
        {
            RuntimeGlobals.LMFTracker.Initialize();
        }

        public void SetConnectModel()
        {
        }

        public void ClearConnectModel()
        {
        }

        //记录所设置的单位
        public void setUnits()
        {
            if (IsTrackerConnected())
            {
                RuntimeGlobals.AngleUnit = RuntimeGlobals.LMFTracker.Settings.Units.AngleUnit.ToString();
                RuntimeGlobals.LengthUnit = RuntimeGlobals.LMFTracker.Settings.Units.LengthUnit.ToString();
            }
        }

        public void Connect(string ip)
        {
            RuntimeGlobals.MainForm.ShowConnectedResults("Connecting...","Red");
            RuntimeGlobals.LMFTracker = RuntimeGlobals.Connection.Connect(ip);
            RuntimeGlobals.MainForm.ShowConnectedResults("Connected to "+RuntimeGlobals.LMFTracker.Name+"!","Yellow");
            RuntimeGlobals.LMFTracker.Disconnected += (t, ex) =>
            {
                RuntimeGlobals.LMFTracker = null;
                RuntimeGlobals.MainForm.ShowConnectedResults("No Tracker Connected!","White");
            };
            setUnits();
        }

        public void Disconnect()
        {
            if (RuntimeGlobals.LMFTracker != null)
            {
                if (RuntimeGlobals.LMFTracker.Measurement.Status.Value == EMeasurementStatus.MeasurementInProgress)
                    RuntimeGlobals.MainForm.StopMeasure_Click(null, new EventArgs());
                //RuntimeGlobals.MainForm.WriteToExcel_Click(null, new EventArgs());
                RuntimeGlobals.LMFTracker.Initialize();
                RuntimeGlobals.LMFTracker.Disconnect();
            }
        }

        public bool IsTrackerConnected()
        {
            return RuntimeGlobals.LMFTracker != null;
        }

        //public bool DisconnectIfConnected()
        //{
        //    if (IsTrackerConnected())
        //    {
        //        Disconnect();
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public void Discover()
        {
            var trackerfinder=new TrackerFinder();
            var trackerinfocol = trackerfinder.Trackers;
            var trackers = new List<TrackerInfo>();

            foreach (var trackerinfo in trackerinfocol)
            {
                if(!trackers.Any(t=>t.IPAddress.Equals(trackerinfo.IPAddress)))
                    trackers.Add(trackerinfo);
            }
            //var sortedtrackers = trackers.Select(t => String.Format("{0}:{1}-{2}", t.IPAddress, t.Type, t.Name))
            //    .ToArray();
            var sortedtrackers = trackers.Select(t => $"{t.IPAddress}:{t.Type}-{t.Name}").ToArray();
            //这里可以访问到主窗体
            RuntimeGlobals.MainForm.SetDiscoveredTrackers(sortedtrackers);
        }
    }
}
