﻿namespace LaserTrackerController
{
    partial class AlignmentAboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.AlignmentAboutClosebt = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(510, 250);
            this.panel1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(7, 7);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(495, 235);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "\r\nAlignment为转站设置选项，可根据测量的点(Actual)及理论点(Nominal)计算得出测量仪Tracker与Object或者WCS的关系（适用于不" +
    "同的测量系统），其中测量仪Tracker与Object的关系计算采用Transformation的计算方式；Tracker与WCS的关系计算采用Orientat" +
    "ion的计算方式。\r\n";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AlignmentAboutClosebt
            // 
            this.AlignmentAboutClosebt.Location = new System.Drawing.Point(442, 270);
            this.AlignmentAboutClosebt.Name = "AlignmentAboutClosebt";
            this.AlignmentAboutClosebt.Size = new System.Drawing.Size(80, 25);
            this.AlignmentAboutClosebt.TabIndex = 1;
            this.AlignmentAboutClosebt.Text = "Close";
            this.AlignmentAboutClosebt.UseVisualStyleBackColor = true;
            this.AlignmentAboutClosebt.Click += new System.EventHandler(this.AlignmentAboutClose_Click);
            // 
            // AlignmentAboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 312);
            this.Controls.Add(this.AlignmentAboutClosebt);
            this.Controls.Add(this.panel1);
            this.Name = "AlignmentAboutDialog";
            this.Text = "AlignmentAboutDialog";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button AlignmentAboutClosebt;
    }
}