﻿namespace LaserTrackerController
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.CloseAboutbt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(510, 250);
            this.panel1.TabIndex = 0;
            // 
            // CloseAboutbt
            // 
            this.CloseAboutbt.Location = new System.Drawing.Point(442, 270);
            this.CloseAboutbt.Name = "CloseAboutbt";
            this.CloseAboutbt.Size = new System.Drawing.Size(80, 25);
            this.CloseAboutbt.TabIndex = 0;
            this.CloseAboutbt.Text = "Close";
            this.CloseAboutbt.UseVisualStyleBackColor = true;
            this.CloseAboutbt.Click += new System.EventHandler(this.CloseAbout_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(7, 7);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(495, 235);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "\r\n该软件基于LMF SDK开发，旨在实现对LMF Tracker的便捷控制，该软件已实现设备连接、运动控制、测量设置、实时测量、目标预览、转站设置及异常输出等功" +
    "能。且调用了Office Excel动态链接库，可将测量结果实时写入Excel表格，实现测量数据的实时记录及显示。";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 312);
            this.Controls.Add(this.CloseAboutbt);
            this.Controls.Add(this.panel1);
            this.Name = "AboutDialog";
            this.Text = "AboutDialog";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button CloseAboutbt;
    }
}