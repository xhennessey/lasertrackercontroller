﻿namespace LaserTrackerController
{
    partial class OVCdialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.OVCpictureBox = new System.Windows.Forms.PictureBox();
            this.StartOVCbt = new System.Windows.Forms.Button();
            this.StopOVCbt = new System.Windows.Forms.Button();
            this.ShowOVCbt = new System.Windows.Forms.Button();
            this.CloseOVCbt = new System.Windows.Forms.Button();
            this.StillImageModecmb = new System.Windows.Forms.ComboBox();
            this.ShowStillImagebt = new System.Windows.Forms.Button();
            this.Closebt = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OVCpictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.OVCpictureBox);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(610, 360);
            this.panel1.TabIndex = 0;
            // 
            // OVCpictureBox
            // 
            this.OVCpictureBox.Location = new System.Drawing.Point(3, 3);
            this.OVCpictureBox.Name = "OVCpictureBox";
            this.OVCpictureBox.Size = new System.Drawing.Size(604, 354);
            this.OVCpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.OVCpictureBox.TabIndex = 0;
            this.OVCpictureBox.TabStop = false;
            // 
            // StartOVCbt
            // 
            this.StartOVCbt.Location = new System.Drawing.Point(101, 385);
            this.StartOVCbt.Name = "StartOVCbt";
            this.StartOVCbt.Size = new System.Drawing.Size(80, 25);
            this.StartOVCbt.TabIndex = 1;
            this.StartOVCbt.Text = "Start OVC";
            this.StartOVCbt.UseVisualStyleBackColor = true;
            this.StartOVCbt.Click += new System.EventHandler(this.StartOVC);
            // 
            // StopOVCbt
            // 
            this.StopOVCbt.Location = new System.Drawing.Point(13, 416);
            this.StopOVCbt.Name = "StopOVCbt";
            this.StopOVCbt.Size = new System.Drawing.Size(80, 25);
            this.StopOVCbt.TabIndex = 2;
            this.StopOVCbt.Text = "Stop OVC";
            this.StopOVCbt.UseVisualStyleBackColor = true;
            this.StopOVCbt.Click += new System.EventHandler(this.StopOVC);
            // 
            // ShowOVCbt
            // 
            this.ShowOVCbt.Location = new System.Drawing.Point(13, 385);
            this.ShowOVCbt.Name = "ShowOVCbt";
            this.ShowOVCbt.Size = new System.Drawing.Size(80, 25);
            this.ShowOVCbt.TabIndex = 3;
            this.ShowOVCbt.Text = "Show OVC";
            this.ShowOVCbt.UseVisualStyleBackColor = true;
            this.ShowOVCbt.Click += new System.EventHandler(this.ShowOVC);
            // 
            // CloseOVCbt
            // 
            this.CloseOVCbt.Location = new System.Drawing.Point(101, 416);
            this.CloseOVCbt.Name = "CloseOVCbt";
            this.CloseOVCbt.Size = new System.Drawing.Size(80, 25);
            this.CloseOVCbt.TabIndex = 4;
            this.CloseOVCbt.Text = "Close OVC";
            this.CloseOVCbt.UseVisualStyleBackColor = true;
            this.CloseOVCbt.Click += new System.EventHandler(this.CloseOVCDialog);
            // 
            // StillImageModecmb
            // 
            this.StillImageModecmb.FormattingEnabled = true;
            this.StillImageModecmb.Location = new System.Drawing.Point(428, 385);
            this.StillImageModecmb.Name = "StillImageModecmb";
            this.StillImageModecmb.Size = new System.Drawing.Size(192, 20);
            this.StillImageModecmb.TabIndex = 5;
            // 
            // ShowStillImagebt
            // 
            this.ShowStillImagebt.Location = new System.Drawing.Point(428, 416);
            this.ShowStillImagebt.Name = "ShowStillImagebt";
            this.ShowStillImagebt.Size = new System.Drawing.Size(109, 25);
            this.ShowStillImagebt.TabIndex = 6;
            this.ShowStillImagebt.Text = "Show Still Image";
            this.ShowStillImagebt.UseVisualStyleBackColor = true;
            this.ShowStillImagebt.Click += new System.EventHandler(this.ShowStillImage);
            // 
            // Closebt
            // 
            this.Closebt.Location = new System.Drawing.Point(548, 416);
            this.Closebt.Name = "Closebt";
            this.Closebt.Size = new System.Drawing.Size(75, 25);
            this.Closebt.TabIndex = 7;
            this.Closebt.Text = "Close";
            this.Closebt.UseVisualStyleBackColor = true;
            this.Closebt.Click += new System.EventHandler(this.CloseDialog);
            // 
            // OVCdialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 447);
            this.Controls.Add(this.Closebt);
            this.Controls.Add(this.ShowStillImagebt);
            this.Controls.Add(this.StillImageModecmb);
            this.Controls.Add(this.CloseOVCbt);
            this.Controls.Add(this.ShowOVCbt);
            this.Controls.Add(this.StopOVCbt);
            this.Controls.Add(this.StartOVCbt);
            this.Controls.Add(this.panel1);
            this.Name = "OVCdialog";
            this.Text = "OVCdialog";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OVCpictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox OVCpictureBox;
        private System.Windows.Forms.Button StartOVCbt;
        private System.Windows.Forms.Button StopOVCbt;
        private System.Windows.Forms.Button ShowOVCbt;
        private System.Windows.Forms.Button CloseOVCbt;
        private System.Windows.Forms.ComboBox StillImageModecmb;
        private System.Windows.Forms.Button ShowStillImagebt;
        private System.Windows.Forms.Button Closebt;
    }
}